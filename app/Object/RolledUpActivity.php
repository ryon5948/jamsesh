<?php 
	class RolledUpActivity
	{
		private $song;
		private $userActivities = array();
		private $userIds = array();
		
		public function getSong()
		{
			return $this->song;
		}
		
		public function setSong($song)
		{
			$this->song = $song;
		}
		
		public function getActivities()
		{
			return $this->userActivities;	
		}
		
		public function addActivity($activity)
		{
			$user = $activity['User'];
			if(empty($user)) return;
			
			if(!isset($this->userIds[$user['id']]))
			{
				$this->userIds[$user['id']] = $user['id'];
				$this->userActivities[$user['id']] = array();
				$this->userActivities[$user['id']]['ListOfActivities'] = array();
				$this->userActivities[$user['id']]['User'] = $user;
			}
			
			array_unshift($this->userActivities[$user['id']]['ListOfActivities'],
				$activity['Activity']);
		}
	}
?>