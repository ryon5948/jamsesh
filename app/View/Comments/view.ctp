
<!-- Generic view for showing comments on any model -->

<?php 

if(empty($comments)){
	echo $this->Session->flash();
}
else {
?>
	<?php foreach($comments as $comment): ?>
	  <div class="media">
	    <div class="media-img pull-left">
	    	<?php echo $this->Html->image($comment['User']['image_url'],array('class'=>'img-responsive')) ?>
	    </div>
	    <div class="media-body">
	      <div>
	        <a href="/users/profile/<?php echo $comment['User']['id'] ?>">
	        	<h4 class="media-heading"><?php echo $comment['User']['stage_name'] ?></h4>
	        </a>
	        <p class="media-timestamp"><?php echo $this->Date->getRelativeDate($comment['Comment']['created']); ?></p>
			<?php echo h($comment['Comment']['content']); ?>
	      </div>
	    </div>
	  </div>
	<?php endforeach;
 } ?>
 
<script>
	offset += <?php echo sizeof($comments); ?>
</script>