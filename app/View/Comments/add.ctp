<?php 
if(isset($comments)){
	echo $this->element('comments',array('comments'=>$comments));
}
else{ ?>
	<div class="alert alert-danger alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<?php echo $this->Session->flash(); ?>
</div>
<?php } ?>