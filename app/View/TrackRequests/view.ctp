<div class="margin-top">
	<div class="panel padding">
		<h2 class="inline-object">
			<?php echo h($track['title']); ?>
			<small>
				by <a href="/users/profile/<?php echo $user['id']?>"><?php echo $user['stage_name'] ?></a>
			</small>
		</h2>
		<small class="text-muted pull-right">
			<?php echo $this->Date->getRelativeDate($track['created']);?>
		</small>
		
		<p class="lead">
			<?php echo h($track['description']); ?>
		</p>
		<?php echo $this->element('single_track',array('track'=>$track)); ?>
		<?php echo $this->element('comments', array('modelType'=>'track','modelId'=>$track['id'])); ?>
	</div>
</div>