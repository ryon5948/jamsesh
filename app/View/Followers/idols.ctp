<?php foreach($followers as $follower): ?>
	<?php $user = $follower['User']; ?>
              <div class="row">
                <div class="col-xs-5" id="profile-avatar"> 
                	<?php echo $this -> Html -> image($user['image_url'],
                	 array('class' => 'img-responsive', 'width' => '150', 'height' => '112', 'alt' => 'avatar')); ?>
				</div>
                <div class="col-xs-7">
                  <div class="profile-data"><small class="label btn-orange3"><?php echo $user['stage_name'] ?></small>
                    <ul class="list-unstyled" style="margin-top:10px">
                      <li><small class="label btn-blue"><?php echo date($dateFormat, strtotime($track['created'])) ?></small></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
<? endforeach ?>
             