<?php
class DateHelper extends AppHelper 
{
	public function getRelativeDate($date) {
		$timeDifference = abs(time() - strtotime($date));
		$output = "";
		
		$days = $timeDifference/(60*60*24);
		
		if($days > 1 && $days < 2)
		{
			$output = floor($days) . ' day ago';
		}
		else if( $days < 1 && $days > 0)
		{
			$output = floor($timeDifference/(60*60)) . ' hours ago';
		}
		else
		{
			$output = floor($days) . ' days ago';
		}
		
		return $output;
    }
}
?>