<div class="row margin-none padding-top">
	<div class="col-sm-4">
		<div class="padding panel">
			<ul>
				<?php 
				$seenUsers = array();
				foreach($messages as $message):
					
				// Prevent the user from seeing themselves in the left nav
				if($message['From']['id'] == $this->Session->read('Auth.User.id')) { 
					$user = $message['To'];
				} else {
					$user = $message['From'];
				} 
					
				if( !isset($seenUsers[$user['id']]) ) {
					$seenUsers[$user['id']] = $user['id']; ?>
				<li>
					<a class="userMessage" data-userId="<?php echo $user['id']; ?>">
						<div class="row">
							<div class="col-xs-2 col-sm-3">
								<?php echo $this->Html->image($user['image_url']); ?>
							</div>
							<div class="col-xs-10 col-sm-9">
								<h4 class="margin-bottom-none">
									<?php echo $user['stage_name']; ?>
									<small>
										<?php echo $this->Date->getRelativeDate($message['Message']['created']); ?>
									</small>
								</h4>
								<p>
									<?php echo $this->Text->truncate(h($message['Message']['content'],30)); ?>
								</p>
							</div>
						</div>
					</a>
				</li>
				<?php } endforeach; unset($message); unset($seenUsers); ?>
			</ul>
		</div>
	</div>
	<div class="col-sm-8 padding-left-none">
		<div class="panel padding">
			<div id="conversation"></div>
				<?php 
				echo $this->Form->create('Message',array('id'=>'message-form','onsubmit'=>'return false;')); ?>
				<div class="form-group">
					<div class="input-group">
				<?php 
				echo $this->Form->input('content',array('id'=>'message','class'=>'form-control','type' =>'text','label'=>false)); 
				echo $this->Form->input('user_id',array('id'=>'recipient','label'=>false,'type'=>'hidden'));
				echo $this->Form->end(); ?>
				<span class="input-group-btn">
					<span class="btn btn-orange" id="message-form-submit">
						<span class="glyphicon glyphicon-comment"></span>
					</span>
				</span>
			</div>
		</div>
	</div>
</div>

<script>

	var	formButton =  $("#message-form-submit");
	var messageInput = $('#message');
	var userMessages = $('.userMessage');
	var messageForm = $('#message-form');
	var recipient = $('#recipient');
	
	function showUserMessages(userId)
	{
		ajaxController.sendRequest(
			'/messages/user/'+userId,
			'GET',
			null,
			function(data){
				$('#conversation').html(data);
			});
	}
	
	function messageSuccess(data) {
		var conversation = $('#conversation');
		conversation.hide().html(data);
		conversation.imagesLoaded(function(){
			conversation.fadeIn('slow');
			messageInput.val("");
		});
	}
	
	// Switch the conversation view when another user
	// is clicked on.
	userMessages.click(function(){
		var userId = $(this).data('userid');
		showUserMessages(userId);
		recipient.val(userId);
	});
	
	// Handle ajax form submits
	formButton.click(function(){
	   ajaxController.sendRequest(
	   	'/messages/add/',
	   	'POST',
	   	messageForm.serialize(),
	   	messageSuccess);
	});
	
	$(function(){
		
		// Show the messages for the first user in the list
		userId = userMessages.first().data('userid');
		showUserMessages(userId);
		recipient.val(userId);
		
		// Enable form submit through enter button
		messageInput.keyup(function(event){
		    if(event.keyCode == 13){
		        formButton.click();
		        return false;
		    }
		});
	});
</script>