
	

<?php 
	foreach( $activities as $activity ): ?>
	<li class="activity-item col-sm-4 col-lg-3 col-xs-6">
		<div class="panel">
			<div class="row">
				<div class="col-sm-12">
					<div class="activity-body">
						<div class="row" style="position:relative">
							<?php $song = $activity->getSong(); ?>
							<a href="/songs/view/<?php echo $song['id']; ?>">
							<?php
								if ($song['image_url']) {
									echo $this->Html->image($song['image_url'], array('width' => '100%'));
								} else {
									echo $this->Html->image($emptyImage, array('width' => '100%'));
								}
							?>
							</a>
							<div class="up-arrow"></div>
						</div>
						<div class="row">
							<div class="col-xs-12 padding-lg">
								<h4>
									<?php echo $this->Html->link(strip_tags($song['title'],'<a>'), '/songs/view/' . $song['id']); ?>
								</h4>
								<p>
									<?php echo $this->Text->truncate(strip_tags($song['description'],'<a>'), 200); ?>
								</p>
								<ul class="social-info">
		            				<li class="padding-right">
										<i class="imoon imoon-heart"></i>&nbsp;&nbsp;<?php echo $song['song_like_count']; ?>
									</li>
									<li>
										<span class="glyphicon glyphicon-comment"></span>&nbsp;&nbsp;<?php echo $song['song_comment_count'] ?>
									</li>
		            			</ul>
							</div>
						</div>
						<?php foreach($activity->getActivities() as $userActivity): ?>
						<div class="row activity-action">
							<div class="col-xs-3">
								<?php $user = $userActivity['User']; ?>
								<a href="/users/profile/<?php echo $user['id'];?>">
								<?php 
								if ($user['image_url'])
									echo $this -> Html -> image($user['image_url']);
								else
									echo $this -> Html -> image('default_profile_icon.gif');
		 						?>
	 							</a>
							</div>
							
							<div class="col-xs-9 padding-right" style="padding-left:0px">
								<p>
								<strong class="activity-title">
									<?php echo $this -> html -> link($user['stage_name'], '/users/profile/' . $user['id']); ?>
								</strong>
								<span class="text-muted">
									<?php 
									$length = sizeof($userActivity['ListOfActivities']);
									foreach($userActivity['ListOfActivities'] as $i=>$activityItem):
										echo strip_tags($activityItem['action'],'<a>');
										if ($i < $length - 1) { echo ", and "; }
										endforeach;
		 							?>
	 							</span>
	 							</p>
							</div>
						</div>
						<?php endforeach ?>
						<div class="margin-top">
							<i class="fa fa-clock-o"></i>
							<?php echo $this->Date->getRelativeDate($activityItem['created']); ?>
						<div class="pull-right">
							<a title="Share this post" onclick="javascript:repost('<?php echo $activityItem['id'] ?>')" class="repost">
								<span class="imoon imoon-share"></span>
							</a>
						</div>
		</div>
				</div>
			</div>
		</div>
		</div>
	</li>
<?php endforeach; ?>
