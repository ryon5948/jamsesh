<!-- Requires that the $user['User'] object be present in the parent element -->
	<span class="btn btn-xs btn-orange follow-user" onclick="javascript:follow(this)" data-user="<?php echo $user['User']['id'] ?>">
		<span class="glyphicons glyphicons-circle_plus"></span>&nbsp;
		<strong><?php echo __("Follow") ?></strong>
	</span>