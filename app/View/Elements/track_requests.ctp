
<div>
	<div class="panel-heading">
		<h4 class="panel-title">Track requests</h4>
		<br/>
	</div>
	<div class="panel-body">
		<?php 
		if($trackRequests) 
		{
			echo $this->element('trackList',
				array(
				'tracks'=>$trackRequests,
				'initialSelect'=>'false',
				'trackType'=>'trackRequests',
				'creator' => $creator )); 
		}
		else { ?>
			 <span class="text-danger">There are currently no requests for this song<span>
		<?php } ?>
	</div>
</div>
