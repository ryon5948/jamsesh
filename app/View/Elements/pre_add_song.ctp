<div class="song-info row">
	<div class="col-lg-5 col-md-6">
		<?php 
			if($song['image_url'])
	 		{
	 			echo $this->Html->image($song['image_url'],array('width'=>'100%')); 
	 		} else {
	 			echo $this->Html->image($emptyImage,array('width'=>'100%'));
	 		}
		?>
	</div>
	<div class="col-lg-7 col-md-6">
		<ul>
			<li>
				<h3><a href="/songs/view/<?php echo $song['id'] ?>"><?php echo $song['title'] ?></a></h3>
			</li>
			<ul class="social-info">
				<li class="padding-right">
					<i class="imoon imoon-heart"></i>&nbsp;&nbsp;<?php echo $song['song_like_count']; ?>
				</li>
				<li>
					<span class="glyphicon glyphicon-comment"></span>&nbsp;&nbsp;<?php echo $song['song_comment_count'] ?>
				</li>
			</ul>
			<li>
				<?php echo $this->Text->autoParagraph($song['description']); ?>
			</li>
		</ul>
	</div>
</div>