<div class="controls padding">
	<span class="btn btn-default">
		<span class="imoon imoon-previous" onclick="javascript:tracksController.resetTracks()"></span>
	</span>
	<span class="btn btn-orange">
		<span class="imoon imoon-play" onclick="javascript:tracksController.playTracks()" ></span>
	</span>
	<span class="btn btn-default">
		<span class="imoon imoon-pause" onclick="javascript:tracksController.pauseTracks()"></span>
	</span>
<br/><br/>
<div class="progress progress-striped">
  <div class="progress-bar progress-bar-warning" id="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
</div>
</div>