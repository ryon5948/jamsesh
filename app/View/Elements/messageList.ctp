<ul>
	<?php foreach($messages as $message): ?>
		<li>
			<div class="row">
				<div class="col-xs-3 col-sm-1 padding-right-none">
					<?php echo $this->Html->image($message['From']['image_url']); ?>
				</div>
				<div class="col-xs-9 col-sm-11">
					<h4>
						<?php echo $message['From']['stage_name']; ?>
						<small><?php echo $this->Date->getRelativeDate($message['Message']['created']);?></small>
					</h4>
					<p>
						<?php echo h($message['Message']['content']); ?>
					</p>
				</div>
			</div>
		</li>
	<?php endforeach; ?>
</ul>
