<div id="message-modal" class="modal fade" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content padding-lg">
			<div class="modal-body" id="modalBody">
				<div class="row">
					<div class="col-xs-2">
						<img id="message-image_url" />
					</div>
					<div class="col-xs-10">
						<h4>
							<span id="message-username"></span>
						</h4>
					</div>
				</div><br/>
				<div class="row">
					<?php 
					echo $this->Form->create('Message',array('action'=>'/add','id'=>'message-form','onsubmit'=>'return false;')); ?>
					<div class="form-group">
						<div class="input-group">
							<?php
							echo $this -> Form -> input('content', array('id'=>'message-content','class' => 'message form-control', 'type' => 'text', 'label' => false));
							echo $this -> Form -> input('user_id', array('id' => 'recipient', 'label' => false, 'type' => 'hidden'));
							echo $this -> Form -> end();
							?>
							<span class="input-group-btn"> 
								<span class="btn btn-orange" id="message-form-submit"> 
									<span class="glyphicon glyphicon-comment"></span> 
								</span> 
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>