
<div>
  <div class="chat-panel">
    <div>
      <?php echo $this->element('comment_form',array('modelName'=>$modelType,'foreignId'=>$modelId)); ?>
    </div>
    <div id="comments" class="panel-body">
    </div>
    <a id="loadComments" class="padding pull-right text-muted" style="display:block;">
    	<span class="glyphicons glyphicons-more"></span>
    </a>
  </div>
</div>
<script>
	var offset = 0;

	$(function(){
		getComments();
		$('#loadComments').click(getComments);
	});
	
	
	// Gets the comments at the current offset.
	// The offset is determined by the number of comments returned
	// in the previous ajax call.
	function getComments()
	{
		ajaxController.sendRequest('/comments/view/<?php echo $modelType ?>/<?php echo $modelId; ?>/'+offset,'GET',null,function(data){
			$('#comments').append(data);
		});
	}
</script>
	