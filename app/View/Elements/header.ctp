<!-- Start: Header -->
<header class="navbar navbar-fixed-top">
	<div class="pull-left"> 
		<a class="navbar-brand" href="/dashboard">
	    	<div class="navbar-logo"><?php echo $this->Html->image('jamsesh.png') ?></div>
	    </a> 

	    <!-- Menu items -->
	    <?php if($this->Session->read('Auth.User.id')) { // Show search form ?> 
	    <ul class="header-nav pull-left">
			<li>
		      	<a ng-href="/explore?sort=popular"> 
		  			Genres 
				</a>
			</li>
		    <li>
				<a ng-href="/users/"> 
			    	Artists
				</a>
		    </li>
		    <li>
				<a ng-href="/songs/add/"> 
					Song
		    	</a>	
		    </li>
    		<li>
    			<a ng-href="/users/songs">
		    		Songs
		    	</a>
    		</li>
    		<li>
    			<a ng-href="/users/tracks">
		    		Tracks
		    	</a>
    		</li>
    		 <li>
				<a ng-href="/messages">
					Messages
				</a>
		    </li>
		</ul>
	</div>
		<?php } ?>
	<div class="pull-right header-btns">
		<?php if(!$this->Session->read('Auth.User.id')) { // Show search form ?> 
			<div class="signin-links">
				<a class="text-blue2" href="/users/login">Login</a>
				|
				<a class="text-light" href="/users/register">Sign up</a>
			</div>
		<?php } else { ?>
			<div class="search-form">
				<small class="glyphicon glyphicon-search"></small>
				<form role="search" type="GET" action="/search" class="search-form">
			    	<input type="text" class="search-bar form-control" name="keyword" placeholder="Search">
			        <button type="submit" class="hide"></button>
			  	</form>
			</div>
		<?php } ?>
		<!-- <div class="social-header">
			<span class="imoon imoon-facebook"></span>
			<span class="imoon imoon-twitter"></span>
		</div> -->
	</div>
</header>

<script>
	// Expand search field on click
	$('#search-icon').click(function(){
		$('.search-form').toggleClass('search-form-expanded');
	});
</script>
<!-- End: Header --> 
