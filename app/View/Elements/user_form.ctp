

<!-- app/View/Users/add.ctp -->

<div class="image-upload-preview text-center">
	<?php if(!$user['image_url']) { ?>
		<div class="image-placeholder">
			<i class="imoon imoon-user2"></i>
		</div>
		<img id="user-image-preview" width="100%">
	<?php } else {
	echo $this->Html->image($user['image_url'],array('width'=>'100%','id'=>'user-image-preview')); 
	} ?>	
</div>

<?php
echo $this->Form->create('User',array('type' => 'file',"enctype" =>"multipart/form-data", 'class' => 'user-edit'));
echo $this->Form->file('image_url',array('style'=>'padding:2px 0px 10px'));
if(!isset($user)){ ?>
	<div class='row'>
		<div class="col-sm-12" style="">
			<?php echo $this -> Form -> input('stage_name', array('class' => 'form-control', 'label' => false, 'placeholder' => __('Stage name'))); ?>
		</div>			
	</div>
	<div class='row'>
		<div class="col-sm-6 padding-right-sm">
			<?php echo $this->Form->input('username', array('class' => 'form-control', 'label' => false, 'placeholder' => __('Username'))); ?>
		</div>
		<div class="col-sm-6 padding-left-sm">
			<?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'placeholder' => __('Password'))); ?>
		</div>			
	</div>
<?php }
echo $this->Form->input('about',array('class'=>'form-control','label' => false,'placeholder'=>'Tell us a little bit about yourself.')); ?>
<div class='row'>
	<div class="col-sm-6" style="padding-right:5px">
		<?php echo $this->Form->input('city',array('class'=>'form-control','label'=>false,'placeholder'=>__('City'))); ?>
	</div>
	<div class="col-sm-6" style="padding-left:5px">
		<?php echo $this->Form->input('state',array('class'=>'form-control','label'=>false,'placeholder'=>__('State'))); ?>
	</div>			
</div>
<?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-orange btn-sm')); ?>
<script>
$("#UserImageUrl").change(function() {
		$('.image-placeholder').hide();
		readURL("user-image-preview",this);
		$('#user-image-preview').removeClass('hide');
   });
</script>
	