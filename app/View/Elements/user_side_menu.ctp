<!-- Tab panes -->
<div class="side-menu border-left">
	<div class="panel">    
		<div class="panel-heading">
			<span class="glyphicons glyphicons-chevron-left"></span>
			<strong class="padding-left"><a href="/songs/add"><?php echo __('Songs'); ?></a></strong>
		</div>
	    <div class="panel-body padding-bottom-none">
	      <ul class="side-menu">
	        <?php if($songs) {
	        foreach($songs as $song): ?>
	            <li>
	            	<div class="row padding-bottom">
	            		<div class="col-md-2">
	            			<a href="<?php echo $baseUrl.'songs/view/'.$song['id'] ?>">
	            			<?php 
	            				if($song['image_url']) {
	            					echo $this->Html->image($song['image_url']);
								}
								else {
									echo $this->Html->image($emptyImage,array('width'=>'100%'));
								} ?>
							</a>
	            		</div>
	            		<div class="col-md-10">
	            			<a href="<?php echo $baseUrl.'songs/view/'.$song['id'] ?>"><?php echo h($song['title']); ?></a>
	            			<br/>
	            			<small class="text-muted">
	            				<?php echo $this->Date->getRelativeDate($song['created']); ?>
	            			</small>
	            		</div>
	            	</div>
	            </li>
	          <?php endforeach;
			   	unset($song);
			    } else {
	          	echo "<span class='text-danger'>You don't currently have any songs</span>";
	          }?>
	      </ul>
	    </div>
	
		<div class="panel-heading">
			<span class="glyphicons glyphicons-chevron-left"></span>
			<strong class="padding-left"><a href="/tracks/add"><?php echo __('Tracks'); ?></a></strong>
		</div>
	    <div class="panel-body">
	      <ul class="side-menu">
	        <?php if($orphanedTracks) {
	        foreach($orphanedTracks as $track): ?>
	            <li>
	            	<a href="<?php echo $baseUrl.'tracks/view/'.$track['id'] ?>">
	            		<?php echo h($track['title']); ?>
	            	</a>
	            	<small class="pull-right text-muted"><?php echo $this->Date->getRelativeDate($track['created']); ?></small>
	            </li>
	          <?php endforeach;
			  unset($track);
	          	} else {
	          		echo "<span class='text-danger'>No tracks to display</span>"; 
	          } ?>
	      </ul>
	    </div>
 
 		<div class="panel-heading">
			<span class="glyphicons glyphicons-chevron-left"></span>
			<strong class="padding-left"><a href="/trackRequest/add"><?php echo __('Track Requests'); ?></a></strong>
		</div>
	    <div class="panel-body">
	      <ul class="side-menu">
	        <?php if($trackRequests) {
	        foreach($trackRequests as $track): ?>
	         <li><strong><a href="<?php echo $baseUrl.'trackRequests/view/'.$track['id'] ?>"><?php echo h($track['title']); ?></a></strong></td>
	          <small class="pull-right text-muted"><?php echo $this->Date->getRelativeDate($track['created']); ?></small>
	          <?php endforeach; ?>
	          <?php } else {
	          	echo "<span class='text-danger'>No track requests to display</span>";
	          }?>
	      </ul>
	    </div>
	</div>
</div>
	
