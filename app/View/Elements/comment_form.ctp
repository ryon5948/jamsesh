<?php echo $this->Form->create('Comment',array('class'=>'form-horizontal','id'=>'comment-form','onsubmit'=>'return false;'));	
?>
<div class="form-group">
	<div class="col-xs-12">
		<div class="input-group">
		<?php 
			echo $this->Form->input('content', array('class' => 'form-control','id'=>'comment-content','type'=>'text','label'=>false,'placeholder'=> __("Comments...")));
			echo $this->Form->input('class', array('type' => 'hidden','value'=> $modelName ));
			echo $this->Form->input('foreign_id', array('type' => 'hidden','value'=> $foreignId )); ?>
			<span class="input-group-btn">
				<span class="btn btn-orange" id="comment-form-submit">
					<i class="fa fa-comment"></i>
				</span>
			</span>
		</div>
	</div>
</div>
<?php echo $this->Form->end(); ?>

<script>
	$(function(){
		
		var formButton =  $("#comment-form-submit");
		var commentInput = $('#comment-content');
		
		commentInput.keyup(function(event){
	    if(event.keyCode == 13){
	        formButton.click();
	        return false;
	    }
		});
		
		function commentSuccess(data) {
			var comments = $('#comments');
			comments.hide().html(data);
			comments.imagesLoaded(function(){
				comments.fadeIn('slow');
				commentInput.val("");
			});
		}
		
		// Handle ajax form submits
		formButton.click(function(){
		   postComment( $('#comment-form').serialize(),commentSuccess );
		});
	});
	
</script>