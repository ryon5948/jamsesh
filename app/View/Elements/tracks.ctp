<div class="panel-body">
	<?php 
	if( $tracks )
	{?>
	    <?php echo $this->element('trackList',
			array('tracks' => $tracks,
			'initialSelect'=>'true',
			'trackType'=>'tracks',
			'creator' => $creator )); ?>
	<?php } else { ?>
		<span class="text-danger">There are currently no tracks in this song</span>
<?php }?>
</div>
