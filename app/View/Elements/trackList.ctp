
<?php foreach ($tracks as $track):
$title = h($track['title']);
$description = h($track['description']);
$url = $track['url'];
$trackId = str_replace(' ','', preg_replace("/[^A-Za-z0-9 ]/", '', $title).$track['id']);
$user = (!empty($track['User'])) ? $track['User'] : array('username'=>'','id'=>'0');
?>
  <div class="row" id="<?php echo $trackId ?>">
    <div class="col-xs-2" id="profile-avatar"> 
    	<?php echo $this->Html->image($user['image_url'],array('width'=>'100%','alt'=>h($user['stage_name'] ))); ?>
	</div>
    <div class="col-xs-10">
    	<div class="pull-right">
			<?php if($this->Session->read('Auth.User.id') == $creator['id']) { ?>
				<a href="/tracks/discard/<?php echo $track['id'] ?>" alt="Delete this track" class="deleteTrack hide">
					<span class="padding-sm imoon imoon-close"></span>
				</a>
				<?php if($trackType == 'trackRequests') { ?>
					<a href="/trackRequests/createTrack/<?php echo $track['id'] ?>" class="addTrack hide" alt="Add track">
						<span class="padding-sm imoon imoon-checkmark"></span>
					</a>
				<?php } ?>
			<?php } ?>
		</div>
		<a href="<?php echo $baseUrl.$trackType.'/view/'.$track['id'] ?>">
			<h4><?php echo $title; ?>
				<span class="pull-right">
					<a href="<?php echo $track['url']; ?>" title="Download">
						<span class="glyphicons glyphicons-download"></span>
					</a>
				</span>
			</h4>
		</a>
		<?php echo $this->Html->para('',$description); ?>
		<div>
			<ul class="social-info">
				<li class="padding-right">
					<i class="imoon imoon-heart"></i>&nbsp;&nbsp;<?php echo $song['song_like_count']; ?>
				</li>
				<li>
					<span class="glyphicon glyphicon-comment"></span>&nbsp;&nbsp;<?php echo $song['song_comment_count'] ?>
				</li>
			</ul>
		</div>
	</div>
 </div>
 <br/>
		

<!-- Initialize the track object -->
<script type="text/javascript">
	var <?php echo $trackId ?> =
		new Track( 
			"<?php echo addslashes($title) ?>",
			"<?php echo $trackId ?>",
			"<?php echo $initialSelect ?>",
			"<?php echo $url ?>");
			
	tracksController.addTrack( <?php echo $trackId ?> );
</script>
<?php endforeach; 
unset($track); ?>

<script>
	// For the alert dialog before discarding a track request.
	function confirm_discard(text) {
	    return confirm(text);
	}
</script>