<div class="jumbotron">
	<h1>
		Welcome to Jamsesh!
	</h1>
	<h3>To get started you can do any of the following:</h3>
	<ul class="padding-left margin-top margin-bottom">
		<li>Create a <strong><a href="/songs/add" class="text-blue">song</a></strong></li>
		<li>Create a <strong><a href="/tracks/add" class="text-orange2">track</a></strong></li>
		<li>Search/Listen to music being made.</li>
		<li>Find an <strong><a href="/users" class="text-orange">artist</a></strong> that you like and follow their music!</li>
	</ul>
</div>
<hr>


