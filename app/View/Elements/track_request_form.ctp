<div class="panel-heading">
	<div class="panel-title">
		<i class="glyphicons glyphicons-note">&nbsp;</i>
		Request a track
	</div>
</div>
<div class="panel-body">
	<div class="col-md-6">
		<?php echo $this->element('recorder'); ?>
	</div>
	<div class="col-md-6">
		<?php echo $this->Form->create('TrackRequest',
			array('type' => 'file',
				'enctype' => 'multipart/form-data',
				'url' => array('controller'=>'trackRequests','action'=>'add',$songId)
				)); ?>
			
				<?php echo $this->Form->input('title',
					array('class'=>'form-control','label'=>false,'placeholder'=>'Title'));
				echo '<br/>';
				
				echo $this->Form->input('description',
					array('rows'=>'1','label'=>false,'class'=>'form-control','placeholder'=>'Description'));
				echo '<br/>';
				
				echo $this->Form->file('url');
				echo $this->Form->hidden('song_id', array('value'=>$songId));
		    	echo "<br>";
				echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-success'));
			  	echo $this->Form->end(); ?> 
	</div>
</div>

