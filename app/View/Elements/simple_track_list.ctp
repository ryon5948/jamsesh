<ul class="song-list">
<?php foreach($tracks as $track): ?>
	<li class="panel">
		<div class="row">
			<div class="col-md-12">
				<div>
					<?php echo $this->element('single_track',array('track'=>$track)); ?>
				</div>	
				
				<ul>
					<li>
						<strong>
							<a href="/tracks/view/<?php echo $track['id'] ?>" ><?php echo $track['title']; ?></a>
						</strong>
					</li>
					<li>
						<ul class="social-info">
							<li class="padding-right">
								<i class="imoon imoon-heart"></i>&nbsp;&nbsp;<?php echo $track['track_like_count']; ?>
							</li>
							<li>
								<span class="glyphicon glyphicon-comment"></span>&nbsp;&nbsp;<?php echo $track['track_comment_count'] ?>
							</li>
						</ul>
					</li>
				</ul>
					
				<p>
					<?php echo $this->Text->truncate($track['description'],200); ?>
				</p>
				
			</div>
		</div>
	</li>
	<br/>
<?php endforeach; ?>
</ul>