<?php
	$trackId = str_replace(' ','', preg_replace("/[^A-Za-z0-9 ]/", '', $track['title']).$track['id']); 
?>
<script type="text/javascript">
	var <?php echo $trackId ?> =
		new Track( 
			"<?php echo h($track['title']); ?>",
			"<?php echo $trackId ?>",
			"true",
			"<?php echo $track['url'] ?>");
			
		tracksController.addTrack( <?php echo $trackId ?> );
</script>

<div class="controls padding">
	<span class="btn btn-default">
		<span class="imoon imoon-previous" onclick="<?php echo $trackId ?>.reset()"></span>
	</span>
	<span class="btn btn-orange">
		<span class="imoon imoon-play" onclick="<?php echo $trackId ?>.play()" ></span>
	</span>
	<span class="btn btn-default">
		<span class="imoon imoon-pause" onclick="<?php echo $trackId ?>.pause()"></span>
	</span>
<br/><br/>
<div class="progress progress-striped">
  <div class="progress-bar progress-bar-warning" id="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
</div>
</div>