<ul>
<?php foreach($songs as $song): ?>
	<li class="panel">
		<div class="row padding">
			<div class="col-md-2">
				<a href="/songs/view/<?php echo $song['id'] ?>" >
				<?php 
					if($song['image_url'])
						echo $this->Html->image($song['image_url']);
					else 
						echo $this->Html->image($emptyImage);
				?>
				</a>
			</div>
			<div class="col-md-10">
				<div>
					<?php if($song['url'])
							echo $this->element('single_track',array('track'=>$song)); ?>
				</div>
				<div>
					<h4>
						<a href="/songs/view/<?php echo $song['id'] ?>" ><?php echo $song['title']; ?></a>
					</h4>
					<?php 
					foreach( explode(",",$song['genre']) as $genre ): ?>
						<a href="/explore/<?php echo $genre; ?>">
							<span class="btn btn-orange genre">
								<strong><?php echo $genre; ?></strong>
							</span>
						</a>
					<?php endforeach; ?>
				</div>
				<br/>
				<div>
					<p>
						<?php echo $this->Text->truncate($song['description'],200); ?>
					</p>
				</div>
			</div>
		</div>
		<ul class="social-info pull-right">
			<li class="border-right">
				<i class="imoon imoon-bubble2"></i>&nbsp;<?php echo $song['song_comment_count'];?>&nbsp;
			</li>
			<li>
				<i class="imoon imoon-heart2"></i>&nbsp;<?php echo $song['song_like_count']; ?>
			</li>
		</ul>
	</li>
<?php endforeach; ?>
</ul>