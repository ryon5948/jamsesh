<div class="modal fade" id="formModal" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modalLabel"></h4>
      </div>
      <div class="modal-body" id="modalBody">
      </div>
    </div>
  </div>
</div>

<script>
function showModal(modalTitle,data) {
	$('#modalBody').html(data);
	$('#modalLabel').text(modalTitle);
	$('#formModal').modal();
}
</script>