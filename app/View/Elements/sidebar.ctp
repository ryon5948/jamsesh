<aside id="sidebar" class="hidden-xs">  	
  <ul class="side-nav">
  	<li>
  		<a class="navbar-brand" href="/users/view">
	    	<div class="navbar-logo"><?php echo $this->Html->image('jamsesh.png') ?></div>
	    </a> 
  	</li>
  	<li>
  		<div class="search-form">
			<form role="search" type="GET" action="/search" class="search-form">
		    	<input type="text" class="search-bar form-control" name="keyword" placeholder="Search">
		        <button type="submit" class="hide"></button>
		  	</form>
		</div>
  	</li>
	<li>
		<ul class="browse-menu">
			<li class="title">
    			Browse
    		</li>
			<li>
		      	<a ng-href="/explore?sort=popular"> 
		  			Genres 
				</a>
			</li>
		    <li>
				<a ng-href="/users/"> 
			    	Artists
				</a>
		    </li>
		</ul>
	</li>
    <li>
    	<ul class="action-menu">
    		<li class="title">
    			Create
    		</li>
    		<li>
    			 <a ng-href="/songs/add/"> 
					Song
			      </a>	
    		</li>
    	</ul>
    </li>
    <li>
    	<ul class="users-menu">
    		<li class="title">
    			YOUR STUFF
    		</li>
    		<li>
    			<a ng-href="/dashboard">
		    		Dashboard
		    	</a>
    		</li>
    		<li>
    			<a ng-href="/users/songs">
		    		Songs
		    	</a>
    		</li>
    		<li>
    			<a ng-href="/users/tracks">
		    		Tracks
		    	</a>
    		</li>
    		 <li>
				<a ng-href="/messages">
					Messages
				</a>
		    </li>
    	</ul>
    </li>   
    <li>
    	<ul class="profile-menu">
    		<li class="title">
    			<span class="imoon imoon-user2"></span>
      			<?php echo $this->Session->read('Auth.User.username'); ?>
    		</li>
    		<li>
				<a href="/users/edit">Edit profile</a>
			</li>
			<li>
				<a href="/users/logout">Sign Out</a>
			</li>	
    	</ul>
    </li>
    <li>
    	<div class="social">
    		<a class="social-links" href="www.facebook.com">
				<span class="imoon imoon-facebook"></span>
			</a>
			<a class="social-links" href="www.twitter.com">
				<span class="imoon imoon-twitter"></span>
			</a>
		</div>
    </li>
  </ul>
</aside>