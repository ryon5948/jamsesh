
<!-- Displays a list of all songs for different users -->
<ul id="song-list" class="song-list">
<?php foreach($songs as $song): 
	if(isset($song['Song'])) {
		$user = $song['User'];
		$song = $song['Song'];
	}
	?>
	<li class="song-item padding">
		<div class="panel">
			<div class="row" style="position:relative">
				<a href="/songs/view/<?php echo $song['id'] ?>" >
				<?php 
					if($song['image_url'])
			 		{
			 			echo $this->Html->image($song['image_url'],array('width'=>'100%')); 
			 		} else {
			 			echo $this->Html->image($emptyImage,array('width'=>'100%'));
			 		}
				?>
				</a>
				<div class="up-arrow"></div>
			</div>
			<div class="row margin-top">
				<div class="col-xs-12">
					<ul>
						<li>
							<h4>
								<a href="/songs/view/<?php echo $song['id'] ?>" ><?php echo h($song['title']); ?></a>
								<small class="text-muted">
									by
									
									<a href="/users/profile/<?php echo $user['id'] ?>">
										<?php echo $user['stage_name']; ?>
									</a>
								</small>
							</h4>
						</li>
					</ul>
				</div>
			</div>
			<div class="panel-footer">
				<ul class="social-info">
					<li class="padding-right">
						<i class="imoon imoon-heart padding-right-sm"></i><?php echo $song['song_like_count']; ?>
					</li>
					<li class="padding-right">
						<span style="display:inline" class="glyphicons glyphicons-comments padding-right-sm"></span><?php echo $song['song_comment_count'] ?>
					</li>
					<li>
						<i class="fa fa-clock-o padding-right-sm"></i>
						<?php echo $this->Date->getRelativeDate($song['created']); ?>
					</li>
				</ul>
			</div>
	</li>
<?php endforeach; ?>

</ul>
<script>
	// Enable masonry for the activity list.
	$(window).load(function(){
		$('#song-list').masonry({
		  columnWidth: 0,
		  itemSelector: '.song-item'
		});		
		
		// This is needed to fix a layout
		// issue with masonry.
		$('#song-list').masonry();
	});
</script>