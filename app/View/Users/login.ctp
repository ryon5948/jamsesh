
<div class="login">
<?php 
// Login form
echo $this -> Form -> create('User', 
	array('action'=> 'login'));
	
	echo $this -> Form -> input('username', 
		array(
			'class' => 'form-control',
		 	'label' => false,
		 	'placeholder'=>'Username',
			'required' => false )); 
			
	echo $this -> Form -> input('password', 
		array(
			'class' => 'form-control',
		 	'label' => false,
		 	'placeholder'=>'Password',
			'required' => false )); 
	echo $this->Form->submit(__('Login'), array('div'=>false,'class'=>'btn btn-orange2 inline-object'));
	echo $this->Form->end(); 
	echo '<br/>';
	
	// Registration link
	echo "Don't have an account yet? ";
	echo $this->Html->link( 'Sign up','/users/register', array('class'=>'text-blue2') );
?>
</div>