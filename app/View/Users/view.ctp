<div class="col-md-12 padding-top">
	<ul id="activities" class="activities-list">
		<div class="padding hide" id="new-user-help">
			<h4>Not following any body yet? <br/>
			Click <a class="text-orange2" href="/users">here</a> to see who has been collaberating on Jamsesh.</h4>
		</div>
	</ul>
	<a id="loadMoreActivities" class="hidden">
		<i class="glyphicons glyphicons-more"></i>
	</a>
</div>

<script>

	var ajaxLoader = $('#ajax-loader');
	var activitiesList = $('#activities');
	activitiesList.hide();
	// Activites ajax
	$(function(){
		getActivities();
	});
	
	// Make ajax call for recent activities
	// Offset is the number of rows currently shown
	function getActivities()
	{	
		function showActivities()
		{
			activitiesList.fadeIn(function(){
				ajaxLoader.hide();
			});
		}
		
		function activitiesSuccess(data)
		{
			
			if(!data.trim() && activitiesList.find('li').length == 0)
			{
				$('#new-user-help').addClass("show");
				activitiesList.fadeIn(function(){
					ajaxLoader.hide();
				});
				return;
			}
			
			if(data)
			{
				activitiesList.append(data);
				showActivities();
				activitiesList.masonry({
					  itemSelector: '.activity-item',
					});	
				activitiesList.imagesLoaded(function(){
					activitiesList.masonry();
				});
			}
			
		}

		ajaxLoader.show();
		// Load all recent activities
		ajaxController.sendRequest(
			'/activities',
			'GET',
			{offset:activitiesList.find('li').length},
			activitiesSuccess);
		
		if(activitiesList.find('li').length >= 10)
		{
			$('#loadMoreActivities').removeClass('hide');
		}
	}
	
	// Remove items and load again.
	$('#refresh').click(function(){
		activitiesList.fadeOut('fast',function(){
			activitiesList.masonry('destroy');
			activitiesList.html("");
			getActivities();
		});	
	});
	
	$('#loadMoreActivities').click(getActivities);
	
	function repost(id)
	{
		if(id)
			ajaxController.sendRequest('/activities/repost/'+id,'POST');
	}
	
</script>