
<?php foreach($users as $user): ?>
	<li class="user-item">
		<div class="row top-row">
			<div class="col-xs-12">
				<a href="/users/profile/<?php echo $user['User']['id'] ?>" >
				<?php 
					if($user['User']['image_url']){
						echo $this->Html->image($user['User']['image_url']);
					}
					else {
						echo $this->Html->image('default_profile_icon.gif');
					}
				?>
				</a>
			</div>
		</div>
		<div class="bottom-row">
			<div class="col-xs-12">
				<div class="">
					<ul class="padding-top user-info">
						<li>
							<strong>
								<a href="/users/profile/<?php echo $user['User']['id'] ?>" >
									<?php echo $user['User']['stage_name']; ?>
								</a>
							</strong>
						</li>
						<?php if($user['User']['city']) { ?>
						<li class="text-muted">
							<span class="imoon imoon-location"></span>
							<?php echo $user['User']['city'] . ', ' . $user['User']['state']; ?>
						</li><br />
						<?php }
						if($user['User']['id'] != $this->Session->read('Auth.User.id')) { ?>
						<li>
							<?php echo $this->element('follow_button',array('user'=>$user)); ?>
							<span onclick="javascript:addUserModalData('<?php echo $user['User']['id'].'\',\''.$user['User']['stage_name'].'\',\''.$user['User']['image_url']; ?>')" class="btn btn-default btn-xs message">
								<i class="fa fa-envelope-o" style="font-weight:bold"></i>
							</span>
						</li>
						<?php } ?>
						<li>
							<p class="lead truncate small">
								<?php echo $user['User']['about']; ?>
							</p>
						</li>
					</ul>
					<small class="panel-info text-muted">
						<?php echo $user['User']['follower_count'];
						echo '&nbsp;';
						echo __('followers'); 
						echo '&nbsp;';
						echo $user['User']['following_count'];
						echo '&nbsp;';
						echo __('following'); ?>
					</small>
				</div>
			</div>
		</div>
	</li>

<?php endforeach; 
echo $this->element('modal'); 
echo $this->element('message_form');?>
</ul>
<script>

	var messageUserName = $('#message-username');
	var messageImageUrl = $('#message-image_url');
	var recipient = $('#recipient');
	var messageModal = $('#message-modal');
	var messageContent = $('#message-content');
	
	// Add user information to the modal dialog box
	// Opted for this over created a separate dialog for 
	// each user.
	function addUserModalData(userId,username,image_url)
	{
		messageUserName.text(username);
		messageImageUrl.attr('src',image_url);
		recipient.val(userId);
		messageContent.val("");
		messageModal.modal();
	}
	// Enable masonry for the activity list.
	$(window).load(function(){
		var users = $('#users');
		users.masonry({
		  columnWidth: 0,
		  itemSelector: '.user-item'
		});
		users.masonry();
		users.css('visibility','visible')
		
		$('.user-item').hoverIntent(function(){
			var topItem = $(this).find('.top-row')[0];
			var bottomItem = $(this).find('.bottom-row')[0];
			var height = '150px';
			$(bottomItem).height(height);
		},
		function(){
			var topItem = $(this).find('.top-row')[0];
			var bottomItem = $(this).find('.bottom-row')[0];
			var height = $(topItem).height();
			$(bottomItem).height('0px');
		});
	
		var	formButton =  $("#message-form-submit");
		// Handle ajax form submits
		formButton.click(function(){
		   ajaxController.sendRequest(
		   	'/messages/add/',
		   	'POST',
		   	$('#message-form').serialize(),
		   	function(){
		   		messageModal.modal('hide');
		   	});
		});
		
		// Enable form submit through enter button
		$('.message').keyup(function(event){
		    if(event.keyCode == 13){
		        formButton.click();
		        return false;
		    }
		});
	
	});
</script>