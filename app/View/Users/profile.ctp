<div class="container padding-top">
<div class="row">
	<div class="col-sm-5 col-md-4">
		<div class="panel">
		<div style="position:relative">
			<?php 
				if($user['User']['image_url']){
					echo $this->Html->image($user['User']['image_url'],array('width'=>'100%'));
				}
				else {
					echo $this->Html->image('default_profile_icon.gif');
					
				}
			?>
			<div class="up-arrow"></div>
		</div>
		<div class="padding">
			<h2 class="inline-object" >
				<?php echo $user['User']['stage_name'] ?><br/>
			</h2>
			<span class="text-muted inline-object">
				<span class="imoon imoon-location"></span>
				<?php echo $user['User']['city'] . ', ' .$user['User']['state']; ?>
			</span>
			<ul class="user-info">
				<li>
					<?php echo $user['User']['follower_count'];?><br/>
					<small><?php echo __('followers'); ?></small>
				</li>
				<li>
					<?php echo $user['User']['following_count'];?><br/>
					<small><?php echo __('following'); ?></small>
				</li>
			</ul>
			<div>
				<?php if($this->Session->read('Auth.User.id') != $user['User']['id']) { ?>
				<span>
					<?php echo $this->element('follow_button'); ?>
				</span>
				<span>
					<span class="btn btn-xs btn-default">
						<i class="fa fa-envelope-o" style="font-weight:bold"></i>
					</span>
				</span>
				<?php } ?>
			</div>
			<div>
				<p class="margin-top">
					<?php echo h($user['User']['about']); ?>
				</p>
			</div>
		</div>
	</div>
	</div>
	<div class="col-sm-7 col-md-8 padding-left-none">
		<?php 
			if( $user['Song'] ) {
				echo $this->element('user_songs',array('songs'=>$user['Song']));
			}
			else {
				echo "<h3 class='padding-lg'>" . __('No songs to display') . "</h3>";
			}?>
	</div>
</div>
</div>
