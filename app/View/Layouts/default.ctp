<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="initial-scale=1.5">
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo "JAMSESH" ?>:
		<?php echo $title_for_layout; ?>
	</title>
	
	<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
	<?php echo $this->Html->css('glyphicons.min'); ?>

	<!-- Core Javascript - via CDN -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/masonry/3.1.2/masonry.pkgd.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular-route.js"></script>

	<base href="/">
	<?php
		echo $this->Html->css('jamsesh');
		echo $this->Html->css('style.min');
		
		//echo $this->Html->script('main');
		echo $this->Html->script('Track');
		echo $this->Html->script('soundmanager2.js');
		echo $this->Html->script('TracksController');
		echo $this->Html->script('app');
		echo $this->Html->script('controllers');
	
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

	
</head>
<body ng-app="jamseshApp">
	<div id="main">
		<section id="content">
			<?php 
			echo $this->element('header');?>			
			<!-- Area for the main content -->
			<div class="main-content">
				<div ng-view>
				</div>
				<?php //echo $this->fetch('content'); ?>
			</div>
		</section>
	</div>
</body>

<?php 
	echo $this->Html->script('custom');
	echo $this->element('modal'); 
	echo $this->element('message_form');
//	if( $this->Session->read('Auth.User.role') == 'admin' )
		//echo $this->element('sql_dump'); 	
	?>
</html>
<script>
	//Core.init();
	var userId = "<?php echo $this->Session->read('Auth.User.id') ?>";
</script>

