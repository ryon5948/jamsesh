<!-- app/View/Users/add.ctp -->
<div class="container-fluid">
	<div class="row homepage">
		<div class="col-sm-10 center-column"> 
			<h1><strong>Listen, Create, Collaborate</strong></h1>
			<span class="lead">Jamsesh is a platform where users can create songs 
				<br/>and collaborate on them with other users from around the world!</span>
		</div>	
	</div>

	<div id="home-info">
		<span id="scroll-down" class="glyphicon glyphicon-chevron-down"></span>
		<span id="scroll-up" class="glyphicon glyphicon-chevron-up"></span>
		<div class="row padding-top-lg">
			<div class="instructions col-sm-10 center-column">
				<div class="col-sm-6 text-right">
					<h1>Songs</h1>
					<p>
						Create a song that's available for everyone to listen to!  
						Once you create a song, your a the sole owner of it and
						can decide which tracks you would like to make the final mix.
					</p>
				</div>
				<div class="col-sm-6">
					<img src="http://www.testmeat.co.uk/photos/images/turntable.jpg" alt="...">
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="instructions col-sm-10 center-column">
				<div class="col-sm-6">
					<img src="http://www.backgroundsy.com/file/large/old-microphone-icon.jpg">
				</div>
				<div class=" col-sm-6">
					<h1>Tracks</h1>
					<p>
						Have the perfect riff in mind? Upload an audio file for a song or just to keep for later and this will be a track!
						Each song consists of multiple tracks from at least one Jamsesh user.
					</p>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="instructions col-sm-10 center-column">
				<div class="col-sm-6 text-right">
					<h1>Requests</h1>
					<p>
						Hear a song that you like?  Think you can make it even more awesome?  
						Upload an audio file and request for it to be added to the mix.
						Any user can request to add any track to any song!
					</p>
				</div>
				<div class="col-md-6">
					<img src="http://img.gawkerassets.com/img/17or6u9nz5df7jpg/ku-xlarge.jpg">
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(function(){
		var scrollUp = $('#scroll-up');
		var scrollDown = $('#scroll-down');
		// Show full screen image
		$('.homepage').height( $(window).height() + 'px' );
		
		// Offset information divs the size of the down arrow
		$('#home-info').css('margin-top',$(window).height() - $('.navbar').height() + 'px' );
		
		scrollUp.click(function(){
			 $("html, body").animate({ scrollTop: 0 }, 600);
        	return false;
		});
		
		$(window).scroll(function(){
			if( scrollDown.offset().top < $(window).scrollTop() )
			{
				scrollUp.show();
			}
			else
			{
				scrollUp.hide();
			}
		});
	});
</script>
