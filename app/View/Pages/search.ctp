<!-- Search header -->

<!-- Songs -->
<div class="col-lg-12">
<div class="sort-header panel">
	<h3>
		<?php echo __('Search');?>
		<span class="text-orange"><?php echo h($keyword);?></span><br/>
		<small class="text-muted">
			<span class="glyphicon glyphicon-sort"></span>
			<a class="text-blue2">Popular</a> / 
			<a>Oldest</a> / 
			<a>Newest</a>
		</small>
	</h3>
</div>
<div>
	<?php
	if(!isset($songs) || !$songs)
		echo '<span class="padding text-danger">'.__('Sorry, but your search did not match any songs!').'</span>';
	else
		echo $this->element('songList',array('songs',$songs));
	?>
</div>
</div>