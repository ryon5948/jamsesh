<div class="col-sm-12">
<div class="sort-header panel">
	<h3>
		 <?php $array = Configure::read('ALL_GENRES'); 
	        	foreach($array as $genre) : ?>
	        	<a class="<?php if(strcasecmp($genre,$selectedGenre) == 0) echo "text-orange"; ?>" 
	        		href="<?php echo $baseUrl.'explore/'.$genre ?>"><?php echo $genre ?></a>&nbsp;
	        	<?php endforeach; ?>
	        	<br/>
		<small class="text-muted">
			<span class="glyphicon glyphicon-sort"></span>
			<a class="explore-link" id="popular" data-sort="popular">Popular</a> / 
			<a class="explore-link" id="oldest" data-sort="oldest">Oldest</a> / 
			<a class="explore-link" id="newest" data-sort="newest">Newest</a>
		</small>
	</h3>
</div>
<?php 
	if($songs)
		echo $this->element('songList',array('songs',$songs));
	else
		echo '<span class="text-danger">'.__('Sorry, but your search did not match any songs!').'</span>';
?>
</div>

<script>
	var currentUrl = window.location.href;
	var sortIndex = currentUrl.indexOf("?sort=");
	var queryParam = currentUrl.substring(currentUrl.indexOf("=")+1);
	var strippedUrl = currentUrl.substring(0, sortIndex);
	
	$('#'+queryParam).addClass('text-blue2');
	
	// Strip any previous "sort" query params
	// and add the current
	$('.explore-link').click(function(){
		window.location.href = strippedUrl + "?sort=" + $(this).data('sort');
	});
</script>