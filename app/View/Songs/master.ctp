
<h1>
	<?php echo $song['Song']['title'] ?>
	<small>by <?php echo $song['User']['stage_name'] ?> on 
		<?php echo date($dateFormat, strtotime($song['Song']['created'])); ?>
	</small>
</h1>

<p class="lead">
	<?php echo h($song['Song']['description']); ?>
</p>
<?php echo $this->element('single_track',array('track'=>$song['Song'] )); ?>
