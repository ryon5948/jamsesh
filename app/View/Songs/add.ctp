<!-- app/View/Songs/add.ctp -->	
<div class="row margin-none padding-top">
	<div class="col-lg-4 col-sm-5 padding-right-none">
		<div class="panel padding">
			<div class="image-upload-preview text-center">
				<img src="<?php echo $emptyImage; ?>" width="100%" id="song-image-preview">
			</div>
			<br/>
	        <?php
	        echo $this->Form->create('Song',array('type'=>'file','enctype'=>'multipart/form-data'));
			echo $this->Form->input('image_url',array('type' =>'file','label'=>false));
			echo '<br/>';
	        echo $this->Form->input('title', array('class' => 'form-control'));
			echo "<br/>";
	        echo $this->Form->input('description',array('type' => 'textarea','class' => 'form-control'));
			echo $this->Form->input('genre',
				array('label'=>false,'class'=>'inline-object genre','name'=>'data[Song][genre]','type'=>'select',
					'multiple'=>'checkbox','options'=>Configure::read('ALL_GENRES')));
	    	
			echo $this->Form->submit(__('SUBMIT'), array('class' => 'btn btn-orange btn-sm','id'=>'song-form-submit'));
			echo $this->Form->end(); ?>
		</div>
	</div>
	<div class="col-sm-7 col-lg-8">
		<div class="panel padding">
			<h3>
				Creating a song?<br/>
				<small>Here's what you need to know:</small>	
			</h3>
			<ul class="info-list">
				<li>
					<p>
						Every song is open to the public.  <strong>ANYBODY</strong> can listen to it!
					</p>
				</li>
				<li>
					<p>
						Every song is open for any user to submit a track request.  As the creator of the song
						you are free to accept or reject that track request.  If you accept, the track will be added 
						to the list of tracks for that song and can be removed at any time.
					</p>
				</li>
				<li>
					<p>
						Once your song is complete, go ahead and <em>master</em> it.  This process involves combining all of the 
						tracks into a single <strong>mp3</strong> file that can be shared and downloaded based on your settings!
					</p>
				</li>
				<li>
					<p>
						A track does not have to be associated with a song in order to be created.
						Tracks can be moved to and from any song, at any time.
					</p>
				</li>
			</ul>
	</div>
	</div>
</div>

</div>
<script>
	$("#SongImageUrl").change(function() {
			if(readURL("song-image-preview",this))
				$('.image-placeholder').hide();
       });
</script>