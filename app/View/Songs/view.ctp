<!-- View a song and all of it's tracks, track requests, and comments -->
<div class="row padding-top margin-right-none margin-left-none">
	<div class="col-sm-8">
		<div class="panel padding">
		<div class="row">
		    <div class="col-sm-5 col-md-4">		    	
		    	<?php 
		      	if($song['image_url'])
		 		{
		 			echo $this->Html->image($song['image_url'],array('width'=>'100%')); 
		 		} else {
		 			echo $this->Html->image($emptyImage,array('width'=>'100%'));
		 		}?>
			</div>
			<div class="col-sm-7 col-md-8 padding-left-none"> 
				<div class="padding">
					<a class="pull-right" alt="Likes">
						<i class="imoon imoon-heart2"></i>&nbsp;<strong><?php echo $song['song_like_count']; ?></strong>
					</a>
					<h3 class="margin-top"><?php echo $song['title'] ?>
						<small>by <?php echo $this->Html->link($creator['stage_name'],'/users/profile/'.$creator['id']); ?></small>
			 		</h3>
			 		<?php 
					foreach( explode(",",$song['genre']) as $genre ): ?>
						<a href="/explore/<?php echo $genre; ?>">
							<span class="btn btn-orange genre">
								<strong><?php echo $genre; ?></strong>
							</span>
						</a>
					<?php endforeach; ?>
					<br/>
					<p>
					<?php echo h($song['description']);?>
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="song-actions">
					<?php if($this->Session->read('Auth.User.id') == $creator['id']) { ?>
						<a class="text-blue2" id="master-song" class="text-blue2">
							<?php echo __('Master'); ?>
						</a> /
						<a href="/tracks/add/<?php echo $song['id'] ?>">
					  		<?php echo __('Add a track'); ?>
						</a>
					<?php } else { ?>
						<a class="text-blue2" href="/trackRequests/add/<?php echo $song['id']; ?>">
							<?php echo __('Make a request'); ?>
						</a> /
						<a id="like">
							<span id="like-text"><?php echo __('Like'); ?></span>
						</a>
					<?php } ?>
				</div>        
				<div class="padding-top"> 					
					<?php echo $this->element('song_controls');
	
					//Displays the section for adding and displaying track requests.			
					// Displays the tracks table
					echo $this->element('tracks',array('tracks'=>$tracks,'creator'=>$creator));
					echo "<br/>";
					echo $this->element('track_requests',array('trackRequests'=>$trackRequests,'creator'=>$creator, 'songId'=>$song['id']));
					?>
				</div>
			</div>
		</div>
		</div>
	</div>	
	<div class="col-sm-4 padding-left-none">
		<div class="panel padding">
			<?php echo $this->element('comments', array('modelType'=>'song','modelId'=>$song['id'])); ?>
		</div>
	</div>
</div>
<?php echo $this->element('modal'); ?>

<script>
	var songId = <?php echo $song['id']; ?>;
	
	$('#master-song').click(function(){
		ajaxController.sendRequest('/songs/master/','POST',{songId:songId},function(){
			window.location.href = "/songs/master/" + songId;
		},function(response){
			showModal(response);
		})
	});
	
	$('#like').click(function(){
		ajaxController.sendRequest('/likes/add/song/<?php echo $song['id']; ?>','POST',null,function(){
			var like = $('#like-text');
			like.text('Liked');
			like.addClass('text-orange2');
		},function(response){
			showModal("We're sorry","It appears that you've already shown your love for this song");
		})
	});
	
</script>
	