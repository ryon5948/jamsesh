<div class="row margin-none">
<div class="col-lg-12">
<div class="panel padding-lg margin-top">
	<h2 class="inline-object">
		<?php echo h($track['title']); ?>
		<small>
			by <a href="/users/profile/<?php echo $user['id']?>"><?php echo $user['stage_name'] ?></a>
		</small>
	</h2>
	<a class="pull-right" alt="Likes">
		<i class="imoon imoon-heart2"></i>&nbsp;<strong><?php echo $song['song_like_count']; ?></strong>
	</a>
	<span>
		from <?php echo $this->Html->link($song['title'],'/songs/view/'.$song['id']); ?>
	</span>
	<br/>
	<small class="text-muted">
		<?php echo $this->Date->getRelativeDate($track['created']);?>
	</small>
	<p class="lead margin-top">
		<?php echo h($track['description']) ?>
	</p>
	<?php echo $this->element('single_track',array('track'=>$track));
	echo "<br/>";
	echo $this->element('comments', array('modelType'=>'track','modelId'=>$track['id'])); ?>
</div>
</div>
</div>