

<!-- app/View/Tracks/add.ctp -->
<div class="row margin-none padding-top">
	<div class="col-sm-4">
		<div class="panel padding">
			<?php 
			echo $this->Form->create('Track',array('type'=>'file',"enctype" =>"multipart/form-data")); 
				if(isset($song)) 
				{
					$songId = $song['id'];
				}
				else {
					$songId = "0";
				}
				echo $this->element('recorder');
				echo '<br/>';
				echo $this->Form->input('url',array('label'=>false,'type'=>'file')); 
				echo '<br/>';
				echo $this->Form->input('title',array('class'=>'form-control'));
			    echo $this->Form->input('description',array('type' => 'textarea','class'=>'form-control','rows'=>'1'));
			    echo $this->Form->hidden('song_id', array( 'value' => $songId ) );
				echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-orange2 btn-md','data-loading-text'=>'Creating...'));
			?>
		</div>
	</div>
	<div class="col-sm-8 padding-left-none">
		<div class="panel padding">
			<?php if(isset($song)) {
					echo $this->element('pre_add_song',array('song'=>$song));		
					echo "<br/><br/>";	
			} ?>
			<h3>
				Uploading tracks?
				<small>Keep these in mind</small>	
			</h3>
			<ul class="info-list">
				<li>
					<p>
						Each track is open to the public.  <strong>ANYBODY</strong> can listen to it!
					</p>
				</li>
				<li>
					<p>
					Max upload file size is <strong>10MB</strong>.
					</p>
				</li>
				<li>
					<p>
					All tracks will be converted to <strong>MP3</strong>.
					</p>
				</li>
				<li>
					<p>
					A track does not have to be associated with a song in order to be created.
					Tracks can be moved to and from any song, at any time.
					</p>
				</li>
			</ul>
		</div>
	</div>
</div>
