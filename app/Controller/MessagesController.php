<?php
class MessagesController extends AppController 
{
	
	public function beforeFilter() 
	{
		parent::beforeFilter();
	}
	
	
	/**
	 * Gets a list of the last message from each unique user to the 
	 * currently logged in user.
	 */
	public function index()
	{
		// Get all followers for the logged in user
		$id = $this->Auth->user('id');
		$messages = $this->Message->getLatestMessagesFromAllUsers($id);
		
		//debug($messages);
		$this->set('messages',$messages);
	}
	
	/**
	 * Retrieve the message conversation between the current user
	 * and the provided user id.
	 * @param the userId for the conversation between the current user
	 */
	public function user($userId)
	{
		$this->layout = 'ajax';
		$this->Message->data['id'] = $this->Auth->user('id');
		$this->set('messagesForSelectedUser',$this->Message->getConversationWithUser($userId));
	}
	
	
	/**
	 * Write a message to the database.
	 */
	public function add()
	{
		$this->layout = 'ajax';
		$userId = $this->request->data['Message']['user_id'];
		if($this->request->isPost())
		{
			$id = $this->Auth->user('id');
			$this->request->data['Message']['from_id'] = $id;
			if(!$this->Message->save($this->request->data))
			{
				throw new ErrorException('Error sending message');
			}
		}
		
		$this->Message->data['id'] = $this->Auth->user('id');
		$this->set('messagesForSelectedUser',$this->Message->getConversationWithUser($userId));
	}
}
?>