<?php
App::uses('UploadFileUtility', 'Model');
class TrackRequestsController extends AppController 
{
	public $recursive = 2;
	public $uses = array('Track','TrackRequest','UploadFileUtility','Song');
	
	public function view($trackId) 
	{
		$track = $this->TrackRequest->find(
			'first', array(
        		'conditions' => array('TrackRequest.id' => $trackId ),
				));
				
		if(!$track) {
			throw new NotFoundException(__('No track request was found'));	
		}
		
		$this->set('user', $track['User']);
		$this->set('track', $track['TrackRequest']);
		$this->set('song', $track['Song']);
	}
	
	public function add($songId) 
	{
		$this->Song->contain();
		$song = $this->Song->findById($songId);
		
		if( empty($song) && $songId != 0 )
		{
			throw new NotFoundException(__('No song found'));
		}
		
		if ($this->request->is('post')) 
		{	
			$data = $this->request->data;
			$data['TrackRequest']['user_id'] = $this->Auth->user('id');
			
			CakeLog::write('debug', 'Upload'.print_r($data, true) );
			// Assemble a unique object key for S3 uploads.
			//$S3OjbectKey = $data['Track']['url']['name'].$songId.$data['Track']['user_id'];
			$url = $this->UploadFileUtility->moveAudioToS3($data['TrackRequest']['url'],
				$this->Auth->user('id'));
			if( !$url )
			{
				$this->Session->setFlash(
					__('An error occurred! Unfortunately the track could not be saved.'), 'flash_error'
				);
			}
			
			$data['TrackRequest']['url'] = $url;
			$this->TrackRequest->create();
			if ($this->TrackRequest->save($data)) 
			{
				$this->Session->setFlash(__('Congradulations! You\'ve just requested for your track to be added to this song'), 'flash_success');
				return $this->redirect('/songs/view/'.$songId );
			}
		}
		else 
		{
			$this->set("song",$song['Song']);	
		}
	}


	public function createTrack($trackId)
	{
		$request = $this->TrackRequest->findById($trackId);

		if(!$request)
			throw new NotFoundException(__('No track was found.'));
			
		// If the user is not the owner of the request, or the requested song
		if($request['TrackRequest']['user_id'] != $this->Auth->user('id') 
			&& $request['Song']['user_id'] != $this->Auth->user('id') )
		{
			throw new ForbiddenException(__('You are not authorized to access this page.'));
		}
		
		// Primary key for this table is auto_increment
		$request['TrackRequest']['id'] = '';
		$this->Track->create($request['TrackRequest']);
		if(!$this->Track->save($request['TrackRequest']))
		{
			$this->Session->setFlash(
				__('An error occurred! Unfortunately the requests could not be accepted.'), 'flash_error'
			);
		}
		
		// Remove track request from table.
		// No need to update S3 since we still want to keep the audio file.
		$this->TrackRequest->delete($trackId);
		
		$this->Session->setFlash(
			__('The track request was successfully added to the song!'), 'flash_success'
		);
		return $this->redirect($this->referer());
	}

	public function discard($trackId)
	{
		$request = $this->TrackRequest->find('first',array('condtions'=>array('id' => $trackId)));
		
		// If the user is not the owner of the request, or the requested song
		if($request['TrackRequest']['user_id'] != $this->Auth->user('id') 
			&& $request['Song']['user_id'] != $this->Auth->user('id') )
		{
			throw new ForbiddenException(__('You are not authorized to access this page.'));
		}
		if($this->TrackRequest->delete($trackId))
		{
			$this->Session->setFlash(
			__('The track request was successfully discarded!'), 'flash_success');
		}
		else
		{
			$this->Session->setFlash(
			__('There was an error discarding the track request:('), 'flash_error');
		}
		return $this->redirect($this->referer());
	}
} 
?>
	