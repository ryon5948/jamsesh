<?php 
class ActivitiesController extends AppController 
{
	var $uses = array('Follower','Activity');
	
	public function beforeFilter() {
		parent::beforeFilter();
	}
	
	
	public function index()
	{
		$this->layout = '';
		$offset = isset($this->params['url']['offset']) ? $this->params['url']['offset'] : 0;
		
	    $this->set('activities', $this->Activity->getLatestFollowingActivities('20',$offset));
		$this->set('_serialize', array('activities'));	
	}
	
	
	public function repost($activityId)
	{
		$this->autoRender = false;
		if($this->request->is('post'))
		{
			$data = array();
			$activity = $this->Activity->findById($activityId);
			if(!$activity)
				throw new NotFoundException();
			
			$data['user_id'] = $this->Auth->user('id');
			$data['action'] = 'shared this';
			$data['song_id'] = $activity['Activity']['song_id'];
			
			$this->Activity->create();
			$this->Activity->save($data);
		}
	}

}
?>
	