<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {


	public $uses = array('Song','Track');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('display');
	}

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function display() {
		
		//$this->layout = 'unregistered';
		
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
			
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
	
	function explore($genre = null)
	{
		// Get all songs if genre is null
		if( !$genre ) {
			$conditions = array('OR' => array(
            'visibility' => 'PUBLIC',
            'visibility' => NULL));
		}
		else {
			$conditions = array('OR' => array(
            'visibility' => 'PUBLIC',
            'visibility' => NULL),
			'AND' => array('FIND_IN_SET(\''. $genre .'\',genre)'));
		}
		
		if(isset($this->params['url']['sort']))
			$sort = $this->params['url']['sort'];
		else
			$sort = "";
		
		// Select the sort method
		$sortBy = "";
		switch($sort) {
			case 'oldest':
				$sortBy = 'Song.created ASC';
				break;
			case 'newest':
				$sortBy = 'Song.created DESC';
				break;
			default:
				$sortBy = 'Song.song_like_count DESC';
		}
				
		$this->Song->recursive = 0;
		$songs = $this->Song->find('all', 
			array('conditions' => $conditions, 
				  'order' => $sortBy,
				  'limit' => 50));
				  
		$this->set( 'songs' , $songs );
		$this->set('selectedGenre', $genre );
		$this->set('_serialize',array('songs','selectedGenre'));
	}

	function search()
	{
		$keyword = $this->params['url']['keyword'];
		$this->Song->recursive = 0;
		
		$songOptions = array(
			'conditions' => array(
			'OR' => array(
			'Song.description LIKE' => '%'. $keyword . '%',
			'Song.title LIKE' => '%'. $keyword . '%'
		)));
				
		$this->set('songs',$this->Song->find('all',$songOptions));
		$this->set('keyword',$keyword);
		$this->set('_serialize',array('song','keyword'));
	}
	
}
