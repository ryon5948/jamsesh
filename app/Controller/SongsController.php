<?php
class SongsController extends AppController 
{
	public $helpers = array('Html', 'Form');
	
	public $uses = array('Song','UploadFileUtility','Comment');
	
	
	/**
	 * View a single song on a page.  This will provide:
	 * Comments
	 * Song info
	 * Song creator info
	 * Tracks 
	 * TrackRequest
	 */
	public function view($songId) 
	{		 
		$data = $this->Song->find('first', 
		array( 'contain' => 
			array(
				'User',
				'Track' => array('User','conditions' => array('Track.song_id' => '0')),
				'TrackRequest' => array ('User')
			),
			'conditions' => array( 'Song.id' => $songId )));
			
		CakeLog::write('SongView', print_r($data,true) );
		if( $data )
		{
			$this->set('tracks',$data['Track']);
			$this->set('song', $data['Song']);	
			$this->set('creator', $data['User']);
			$this->set('trackRequests', $data['TrackRequest']);
			$this->set('_serialize',array('tracks','song','creator','trackRequests'));
		}
		else 
		{
			$this->Session->setFlash(
					__('Sorry, but the song that you requested does not exist :('), 'flash_error'
			);
			throw new NotFoundException();
		}
	}
	
	public function add() 
	{
		if ($this->request->is('post')) 
		{
			$this->request->data['Song']['user_id'] = $this->Auth->user('id');
			if(isset($this->request->data['Song']['genre']) && $this->request->data['Song']['genre']) {
				$this->request->data['Song']['genre'] = implode(",", $this->request->data['Song']['genre']);
			}
			CakeLog::write('genre', print_r($this->request->data,true) );
			
			$fileName = $this->Song->uploadImage($this->request->data['Song'],$this->Auth->user('id'));
			if(!$fileName)
			{
				return $this->Session->
					setFlash("We were unable to upload you image.   Please ensure that it is less than 4MB before continuing",
					'flash_error');
			}
			
			CakeLog::write('debug',$fileName);
			$this->request->data['Song']['image_url'] = $fileName;
			
			$this->Song->create();
			if ($this->Song->save($this->request->data))
			{
				$this->Session->setFlash(__('You have successfully created a song'),'flash_success');
				return $this->redirect('/songs/view/'.$this->Song->id );
			}
			else 
			{
				$this->Session->setFlash(
					__('We\'re sorry! Unfortunately your song could not be created'), 'flash_error'
				);
			}
		}
	}
	
	// Take all of the corresponding tracks and merge them into one.
	public function master($songId)
	{
		$this->Song->contain('Track','User');
		if(!$songId){
			$songId = $this->request->data('songId');
		}
		$song = $this->Song->findById($songId);
		
		if($this->request->is('post'))
		{
			if(!$song)
			{
				throw new NotFoundException("Song was not found");
			}
			if($song['Song']['user_id'] != $this->Auth->user('id'))
			{
				throw new ForbiddenException(__('You are not authorized to perform this action'));
			}
			
			$this->Song->data = $song;
			$output = $this->Song->master();
			
			$this->Song->id = $songId;
			$song = $this->Song->saveField('url',$output);
			$this->set('song',$song);
		}
		
		$this->set('song',$song);
	}
}
?>