
<?php 
class FollowersController extends AppController 
{
	
	public function beforeFilter() 
	{
		parent::beforeFilter();
	}
	
	
	public function idols()
	{
		// Get all followers for the logged in user
		$id = $this->Auth->user('id');
		$followers = $this->Follower->find('all',
			array( 'contain' => 'User',
			'conditions' => array('User.id' => $id)));
		$this->set('followers',$followers);
	}
	
	public function groupies()
	{
		// Get all followers for the logged in user
		$id = $this->Auth->user('id');
		$followers = $this->Follower->find('all',
		array( 
			'contain' => 'User',
			'conditions' => array('Follower.follower_id' => $id)));
		
		$this->set('followers',$followers);
	}
	
	// Logged in user wants to follow given user id.
	public function follow()
	{
		if ($this->request->is('post')) 
		{
			$this->layout = 'ajax';
			$userId = $this->request->data('userId');
			
			if( $this->Auth->user('id') == $userId )
			{
				throw new NotFoundException();
			}
			
			$data = Array();
			$data['user_id'] = $userId;
			$data['follower_id'] = $this->Auth->user('id');
			
			$follow = $this->Follower->save($data);
			if( !$follow )
			{
				throw new ErrorException();
			}
		}
	}
	
	public function remove($userId)
	{
		$this->layout = 'ajax';
		$this->Follower->user_id = $this->Auth->user('id');
		$this->Follower->follower_id = $userId;
		if( $this->Follower->delete() )
		{
			$this->set('status','failed');
		}
		else
		{	
			$this->set('status','success');	
		}
	}
}
	