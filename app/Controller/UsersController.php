<?php
class UsersController extends AppController {

	public $uses = array('User','UploadFileUtility');
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('register','logout','login');
		$this->User->contain();
	}
	
	// Show all users
	public function index($sortBy='popular') {
		
		$sortParameters = array();
		
		// Handle different sort parameters
		if( $sortBy == 'newest' ) {
			$sortParameters['User.created'] = 'DESC';
		}
		else if( $sortBy == 'oldest' ) {
			$sortParameters['User.created'] = 'ASC';
		}
		else {
			$sortParameters['User.follower_count'] = 'DESC';
		}
		
		
		$users = $this->User->find('all',
			array('order' => $sortParameters,
				  'limit' => '50',
				  'fields' => array('image_url', 'follower_count','stage_name','id',
				  				'city','state','about','follower_count','following_count') ));
			
		$this->set('users',$users);
		$this->set('_serialize', array('users'));
	}

	public function view() {
		
		$id = $this->Auth->user('id');
		if( !$id )
		{
			throw new ForbiddenException(__('You are not authorized to access.'));
		}
		
		/**
		 * Finds:
		 * 		User - by $id
		 * 		Song - all songs created by User
		 * 		Tracks - all Tracks created by User, not in a song owned by user
		 */
		$user = $this->User->find('first',
			array( 'contain' => array(
				'Song' => array(
					'order' => array('Song.created' => 'DESC'),
					'limit' => '5'),
				'Track' => array(
					'order' => array('Track.created' => 'DESC'),
					'limit' => '5'),
				'TrackRequest' => array(
					'order' => array('TrackRequest.created' => 'DESC'),
					'limit' => '5')
				),
			'conditions' => array( 'User.id' => $id )));
		if (!$user) {
			throw new NotFoundException(__('Invalid user'));
		}
		
		$this->set('songs', $user['Song']);
		$this->set('orphanedTracks',$user['Track']);
		$this->set('trackRequests', $user['TrackRequest']);
		$this->set('user', $user['User']);
	}

	public function register() {
		
		if ($this->request->is('post')) {
			
			// Check if user already exists
			$user = $this->User->find('first', array('conditions'=> 
				array('username'=>$this->data['User']['username']),
					  'password'=>$this->User->getEncryptedPassword($this->data['User']['password'])));
			if( isset($user['User']) )
			{
				$this->Session->setFlash(__('We\'re sorry, but this user name is already taken!'), 'flash_error');
				return;
			}
						
			$data = $this->request->data['User'];
			$image = $data['image_url'];
			unset($data['image_url']);
			$this->User->create();
			if (!$this->User->save($data)) {
				$this->Session->setFlash(
						__('The user could not be saved. Please, try again.'), 'flash_error'
				);
				return;
			}
			
			$data['id'] = $this->User->getInsertId();
			$this->User->id = $data['id'];
			if( !empty($image['name']) )
			{
				$data['image_url'] = $this->User->uploadNewImage($image);
				if($this->User->saveField('image_url', $data['image_url']))
				{
					$this->Session->setFlash('Welcome to Jamsesh!','new_user');
					if ($this->Auth->login()) {
            			return $this->redirect($this->Auth->redirectUrl());
					}
				}
				else{
					$this->Session->setFlash('We\'re sosrry, but there was an error uploading your image.','flash_error');	
				}
			}
		}
	}

	public function edit() {
		$user = $this->User->find('first', array(
        	'conditions' => array('User.id' => $this->Auth->user('id'))
    	));
		$this->User->data = $user;
		$this->User->id = $this->Auth->user('id');
		
		// Don't allow people to submit form with username and 
		// have it changed.
		unset($this->request->data['User']['username']);
		
		if ($this->request->is('post') || $this->request->is('put')) {
			
			// Upload user image
			$data = $this->request->data['User'];
			$data['id'] = $user['User']['id'];
			
			// User is trying to upload a new image
			if( !empty($data['image_url']['name']) )
			{
				$data['image_url'] = $this->User->uploadNewImage($data['image_url']);
			}
			else 
			{
				// If no new image is being uploaded, set the image url in the data object to the
				// existing image url.
				$data['image_url'] = $user['User']['image_url'];					
			}
			
			// Unset password to prevent it from being hashed twice
			unset($data['password']);
			unset($this->User->data['User']['password']);
			$user = $this->User->save($data);
			if ($user) {
				$this->Session->setFlash(__('The user has been saved'), 'flash_success');
			}
			else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'),'flash_error');
			}
						
			$this->set('user', $user['User']);
		// Request is GET
		} else {
			$this->request->data = $user;
			$this->set('user',$user['User']);
			unset($this->request->data['User']['password']);
		}
	}

	public function delete($id = null) {
		$this->request->onlyAllow('post');

		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}
	
	public function login() {
		if ($this->request->is('post')) {
			
			if ($this->Auth->login()) {
				return $this->redirect($this->Auth->redirect());
			}
			else {
				throw new NotFoundException("Invalid username/password");				
			}
		}
	}

	public function tracks()
	{
		$this->set('user',$this->User->tracks());
	}
	
	public function trackRequests()
	{
		$this->set('user',$this->User->trackRequests());
	}
	
	public function songs($limit)
	{
		$this->User->id = $this->Auth->user('id');
		$this->set('user',$this->User->songs($limit));
		$this->set('_serialize',array('user'));
	}

	
	public function profile($id=null) {

		/**
		 * Finds:
		 * 		User - by $id
		 * 		Skills
		 * 		Activities
		 * 		Followers
		 * 		Following
		 */
		
		if( !$id )
		{
			$id = $this->Auth->user('id');
		}
		if( !$id )
		{
			throw new ForbiddenException(__('You must be logged in to view this profile.'));
		}
		
		// Get user info and mastered songs
		$user = $this->User->find('first',
			array( 'contain' => array(
				'Song' => array('conditions'=>array('Song.url IS NOT NULL'))
				),
			'conditions' => array( 'User.id' => $id )));
		if (!$user) {
			throw new NotFoundException(__('That user does not exist!'));
		}
		
		$this->set('user',$user);
	}
	
	
	public function logout() {
		return $this->redirect($this->Auth->logout());
	}

}
?>