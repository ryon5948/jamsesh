<?php

/**
 * This class is a little bit strange.  To avoid duplicating code, 
 * and having all comments mashed into one table.
 * we have one controller that handle commenting on multiple models ( Song and Track ).
 * In order for this to happen
 */
class CommentsController extends AppController
{
	public $helpers = array('Html', 'Form');
	public $uses = array('SongComment','TrackComment','Comment');
	
	private function setCommentModel($modelName) {
		switch ($modelName) {
		    case 'song':
		        return $this->SongComment;
		    case 'track':
		       return $this->TrackComment;
		    case 'trackRequest':
		        break;
		}
	}
		
	public function add()
	{
		$this->layout = 'ajax';
		if($this->request->is('post')) {
			
			$modelName = $this->request->data['Comment']['class'];
			
			$this->request->data['Comment']['user_id'] = $this->Auth->user('id');
			$comment = $this->Comment->save($this->request->data);
			if(!$comment)
			{
				$this->Session->setFlash(__('There was an error while trying to add a comment'));
			}
			else{
				$this->set('comments',$this->Comment->getCommentsForModel(
					$modelName,$comment['Comment']['foreign_id'],'0'));
				$this->render('view');
			}
		}
	}
	
	public function view($modelName,$modelId,$offset=0)
	{
		$this->layout = 'ajax';
		CakeLog::write('comments',print_r($modelName + $modelId, true));
		$this->set('comments',$this->Comment->getCommentsForModel($modelName,$modelId,$offset));
		$this->set('_serialize',array('comments'));
	}
}
?>
