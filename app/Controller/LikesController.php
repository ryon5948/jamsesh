<?php
class LikesController extends AppController
{	
	public function add($modelName,$foreignId = 0)
	{
		$this->layout = 'ajax';
		
		if(!$modelName || !$foreignId)
		{
			$this->set('success',"failed");
			return;
		}
		
		if($this->request->is('post')) {
			$data = array();
			$data['Like']['user_id'] = $this->Auth->user('id');
			$data['Like']['foreign_id'] = $foreignId;
			$data['Like']['class'] = $modelName;
			$like = $this->Like->save($data);
			if($like)
				$this->set('success',true);
			else 
				$this->set('success',"failed");
		}
	}
	
	public function view($modelName,$modelId,$offset=0)
	{
		$this->layout = 'ajax';
		CakeLog::write('debug','Comments'.print_r($modelName.$modelId.$offset,true));
		$this->set('comments',$this->Comment->getCommentsForModel($modelName,$modelId,$offset));
	}
}
?>
