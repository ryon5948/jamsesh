
<?php
class TracksController extends AppController 
{
	public $helpers = array('Html', 'Form');
	
	public $uses = array(
        'UploadFileUtility',
        'Song',
        'Track'
    );
	
	public function beforeFilter() {
		parent::beforeFilter();
	}
	
	public function view($trackId) 
	{
		$track = $this->Track->find(
			'first', array(
        		'conditions' => array('Track.id' => $trackId )));
		CakeLog::write('debug', 'Track'.print_r($track, true) );
		
		if(empty($track))
		{
			throw new NotFoundException(__('No track found.'));
		}
		
		$this->set('user', $track['User']);
		$this->set('track', $track['Track']);
		$this->set('song', $track['Song']);
	}
	
	
	public function add($songId = 0) 
	{
		$this->Song->contain();
		$song = $this->Song->findById($songId);
		
		if( empty($song) && $songId != 0 )
		{
			throw new NotFoundException(__('No song found'));
		}
		else if(isset($song['Song']) && $song['Song']['user_id'] != $this->Auth->user('id'))	
		{
			throw new ForbiddenException(__('You are not authorized to perform this action'));
		}
		
		if ($this->request->is('post')) 
		{	
			$data = $this->request->data;
			$data['Track']['user_id'] = $this->Auth->user('id');
			
			CakeLog::write('debug', 'Upload'.print_r($data, true) );
			// Assemble a unique object key for S3 uploads.
			//$S3OjbectKey = $data['Track']['url']['name'].$songId.$data['Track']['user_id'];
			$url = $this->UploadFileUtility->moveAudioToS3($data['Track']['url'],
				$this->Auth->user('id'));
			if( !$url )
			{
				$this->Session->setFlash(
					__('An error occurred! Unfortunately the track could not be saved.'), 'flash_error'
				);
			}
			
			$data['Track']['url'] = $url;
			$this->Track->create();
			if ($this->Track->save($data)) 
			{
				$this->Session->setFlash(__('Track was successfully added!'), 'flash_success');
				return $this->redirect('/tracks/view/'.$this->Track->id );
			}
		}
		else 
		{
			if(isset($song['Song'])) {
				$this->set("song",$song['Song']);					
			}
		}
	}
	
	
	public function discard($trackId)
	{
		$request = $this->Track->findById($trackId);
		
		// If the user is not the owner of the request, or the requested song
		if($request['Track']['user_id'] != $this->Auth->user('id') 
			&& $request['Song']['user_id'] != $this->Auth->user('id') )
		{
			throw new ForbiddenException(__('You are not authorized to access this page.'));
		}
		if($this->Track->delete($trackId))
		{
			$this->Session->setFlash(
			__('The track request was successfully discarded!'), 'flash_success');
		}
		else
		{
			$this->Session->setFlash(
			__('There was an error discarding the track request:('), 'flash_error');
		}
		return $this->redirect($this->referer());
	}
}
?>