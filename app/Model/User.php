<?php
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
App::uses('UploadFileUtility', 'Model');

// app/Model/User.php
class User extends AppModel {
	
	public $actsAs = array('Containable');
	
	public $cacheQueries = true;
	
	var $hasMany = array(
        'Song' => array(
            'className' => 'Song'),
        'Track' => array(
			'className' => 'Track'),
		'TrackRequest' => array(
			'className' => 'TrackRequest' ),
		'Followers' => array(
			'className' => 'Follower'),
		'Following' => array(
			'className' => 'Follower'),
		'Activity' => array(
			'className' => 'Activity')	
    );
	
	public $validate = array(
		'username' => array(
				'required' => array(
					'rule' => array('notEmpty'),
					'message' => 'A username is required'
				)
		),
		'password' => array(
				'required' => array(
					'rule' => array('notEmpty'),
					'message' => 'A password is required'
				)
		)
	);
	
	
	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$passwordHasher = new SimplePasswordHasher();
			$this->data[$this->alias]['password'] = $passwordHasher->hash(
					$this->data[$this->alias]['password']
			);
		}
		return true;
	}
	
	public function getEncryptedPassword($password)
	{
		$passwordHasher = new SimplePasswordHasher();
		return $passwordHasher->hash($password);
	}
	
	
	public function tracks()
	{
		return $this->find('first',
			array( 'contain' => array(
					'Track' => array(
						'order' => array('Track.created' => 'DESC')))));
	}
	
	public function trackRequests()
	{
		return $this->find('first',
			array( 'contain' => array(
				'TrackRequest' => array(
					'order' => array('TrackRequest.created' => 'DESC')))));
	}
	
	public function songs($limit)
	{
		$this->Song->contain();
		return $this->Song->find('all',
			array( 
				'conditions' => array('user_id'=>$this->id),
				'order' => array('Song.created' => 'DESC'),
				'limit' => $limit ));
	}
	
	
	// Delete previous user image, and upload the new one
	public function uploadNewImage($image)
	{
		$deleteSuccess = true;
		$fileUploader = new UploadFileUtility();
		$newImageUrl = "";
		
		if(!empty($this->data['User']['image_url']))
		{
			// Delete previous user image
			$imageKey = substr(strrchr($this->data['User']['image_url'], "/"), 1);
			$fileUploader->deleteImage($imageKey);
		}
		
		if($deleteSuccess) 
		{
			$newImageUrl = 
				$fileUploader->moveImageToS3($image,$this->id);	
		}
		
		// Error uploading image to S3
		if(empty($newImageUrl)){
			// Warn user that the image upload was unsuccessful,
			// but continue with saving the other information.
			$newImageUrl = $this->data['User']['image_url'];
		}
		
		return $newImageUrl;
	}
	
}
?>