<?php 
class Message extends AppModel
{
	public $belongsTo = array(
		'From' => array(
			'className'=>'User',
			'foreignKey' => 'from_id'),
		'To' => array(
			'className'=>'User',
			'foreignKey' => 'user_id'));


	function getConversationWithUser($userId)
	{
		$id = $this->data['id'];
		return $this->find('all',
			array( 
			'order' => array('Message.created'=>'ASC'),
			'limit' => 50,
			'conditions' => 
				array(
				'OR' => array( 
					array('Message.user_id' => $id,
					  'Message.from_id' => $userId ),
					array('Message.user_id' => $userId,
					  'Message.from_id' => $id )))));	
	}
	
	
	function getLatestMessagesFromAllUsers($userId)
	{
		return $this->query(
		"SELECT `Message`.`id`, `Message`.`user_id`, `Message`.`from_id`, `Message`.`created`, 
		`Message`.`content`, `From`.`id`, `From`.`image_url`, `From`.`stage_name`, 
		`To`.`id`,`To`.`image_url`,
		`To`.`stage_name` FROM `jamsesh`.`messages` AS `Message` LEFT JOIN `jamsesh`.`users` AS `From` 
		ON (`Message`.`from_id` = `From`.`id`) LEFT JOIN `jamsesh`.`users` AS `To` ON (`Message`.`user_id` = `To`.`id`)
		RIGHT JOIN (SELECT MAX(created) as last_created, user_id FROM messages GROUP BY user_id)
		as latest ON Message.created = latest.last_created AND Message.user_id = latest.user_id WHERE ((`Message`.`user_id` = ".$userId.") 
		OR (`Message`.`from_id` = ".$userId.")) GROUP BY from_id, user_id ORDER BY 
		`Message`.`created` DESC");
	}
}
