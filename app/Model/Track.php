<?php 

class Track extends AppModel
{
	var $title;
	
	var $belongsTo = array(
        'Song' => array(
            'className' => 'Song',
            'foreignKey' => 'song_id'
        ),
        'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id')
    );
	
	var $hasMany = array(
		'Comment' => array(
			'className' => 'Comment',
			'foreignKey' => 'foreign_id'));
	
	public $validate = array(
        'title' => array(
        	'notEmpty' => array(
                'rule'     => 'notEmpty',
                'required' => "create",
            'message'  => 'This field must be filled out.'
            ),
            'between' => array(
                'rule'    => array('between', 1, 100),
                'message' => 'Between 5 to 15 characters'
            )
        ),
        'description' => array(
            'notEmpty' => array(
                'rule'     => 'notEmpty',
                'required' => "create",
                'message'  => 'This field must be filled out.'
            )
        )
    );
	
	// Add an activity entry for track creation
	public function afterSave($created, $options = Array()) 
	{
		$data = Array();
		$data['user_id'] = CakeSession::read("Auth.User.id");
		$data['song_id'] = $this->data['Track']['song_id'];
		
		$link = "<a href='/tracks/view/".$this->data['Track']['id']."'>%s</a>";
		$data['action'] = "added track " . sprintf($link,$this->data['Track']['title']) . " to this song";
	    $Activity = ClassRegistry::init('Activity');
		$Activity->save($data);
	}
}

?>