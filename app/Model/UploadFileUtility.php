<?php
require APP . '/Vendor/autoload.php';
use Aws\S3\S3Client;

class UploadFileUtility {
	
	public $useTable = false;
		
	private function stripFileExtension($fileName)
	{
		$path_parts = pathinfo($fileName);
		return $path_parts['filename'];
	}
	
	private function transcodeToMp3($fileName,$tmpDir,$type)
	{
		// Don't bother transcoding if the file is already in mp3 format
		if( $type != 'audio/mp3' )
		{
			// convert all uploaded files to mp3 for now
			$newFileName = $this->stripFileExtension($fileName).'.mp3';
			exec('sox '.escapeshellarg($tmpDir.DS.$fileName).' '.escapeshellarg($tmpDir.DS.$newFileName));	
			return $newFileName;
		}
		return $fileName;
	}
	
	// // Only for devo purposes
	// function uploadFile($tmpDir,$fileName,$destinationPath)
	// {
		// CakeLog::write('debug', $_SERVER['DOCUMENT_ROOT'].$destinationPath.$fileName );
		// if(!move_uploaded_file($tmpDir, 
				// $_SERVER['DOCUMENT_ROOT'].$destinationPath.$fileName))
		// {
			// print_r(error_get_last());
			// return FALSE;
		// }
// 
		// return $fileName;
	// }
// 	
	// function moveFileToDirectory($trackData)
	// {
		// $fileName = $trackData['name'];
		// $tmpDir = $trackData['tmp_name'];
		// $type = $trackData['type'];
// 		
		// $fileName = $this->transcodeToMp3($fileName,$tmpDir,$type);
// 		
		// return $this->uploadFile($tmpDir,$fileName,'/app/webroot/files/');
	// }
// 	
	// function moveImageToDirectory($data)
	// {
		// CakeLog::write('debug', print_r($data,true) );
		// return $this->uploadFile($data['image_url']['tmp_name'],$data['image_url']['name'],'/app/webroot/img/');
	// }
	
	function moveAudioToS3($data,$id)
	{
		$fileName = $data['name'];
		$file = $data['tmp_name'];
		$type = $data['type'];

		$fileName = $this->transcodeToMp3($fileName,$file,$type);
		$aws_config = include APP . 'Config/config.php';
		
		// File keys are stored as <userId>_<filename>
		return $this->moveFileToDirectoryAWS($userId . '_' . $fileName,
			$file,
			$aws_config['audio_bucket'],'audio/mpeg');
	}
	
	// $id is used to keep amazon s3 keys unique.
	// For user images user user_id, for song images use song_id.
	function moveImageToS3($data,$id)
	{
		$fileName = $data['name'];
		$file = $data['tmp_name'];
		$filesize = filesize($file);
		
		if($filesize > 4000000 )
		{
			return false;	
		}
		
		$aws_config = include APP . 'Config/config.php';
		return $this->moveFileToDirectoryAWS($id . '_' . $fileName,
			$this->compress_image($file,$file),
			$aws_config['image_bucket'], 'image/jpeg');
	}

	
	function deleteImage($imageKey)
	{
		$aws_config = include APP . 'Config/config.php';
								
		// Instantiate the client.
		$s3 = S3Client::factory(array(
		    'key'    => $aws_config['aws_key'],
		    'secret' => $aws_config['aws_secret']
		));
		
		// If deleteObject fails, an exception will be thrown 
		try
		{
			$result = $s3->deleteObject(array(
			    'Bucket'       => $aws_config['image_bucket'],
			    'Key'          => $imageKey
			));
		} 
		catch(Exception $e)
		{
			return false;
		}
		
		return true;
	}
	
	
	// Uploads the specified source to the specified S3 bucket with
	// the specified name.
	function moveFileToDirectoryAWS($fileName,$source,$bucket,$contentType)
	{
		$aws_config = include APP . 'Config/config.php';
								
		// Instantiate the client.
		$s3 = S3Client::factory(array(
		    'key'    => $aws_config['aws_key'],
		    'secret' => $aws_config['aws_secret']
		));
		
		
		// Upload a file.
		$result = $s3->putObject(array(
		    'Bucket'       => $bucket,
		    'Key'          => $fileName,
		    'ContentType'  => $contentType,
		    'Body'   	   => fopen($source,'r'),
		    'ACL'        => 'public-read'
		));
		
		return $s3->getObjectUrl($bucket,$fileName);
		
	}
	
	
	// Gets the specified audio objects from s3 and
	// returns the file paths into an array
	function getAudioFilesFromAWS($keyNames)
	{
		if(empty($keyNames))
			return null;
		
		$aws_config = include APP . 'Config/config.php';
								
		// Instantiate the client.
		$s3 = S3Client::factory(array(
		    'key'    => $aws_config['aws_key'],
		    'secret' => $aws_config['aws_secret']
		));
		
		$result = array();
		foreach($keyNames as $keyName):
			
			$filePath = '/tmp/' . $keyName;
			
			// Get the object
		    $result = $s3->getObject(array(
		        'Bucket' => $aws_config['jamsesh-audio'],
		        'Key'    => $keyName,
		        'SaveAs' => $filepath
		    ));
			
			array_push($result, $result['Body']->getUri());
		endforeach;
		
		return $result;
	}
	
	
	function compress_image($source_url,$destination_url) {

		$info = getimagesize($source_url);

		if ($info['mime'] == 'image/jpeg')
    		$image = @imagecreatefromjpeg($source_url);

		elseif ($info['mime'] == 'image/gif')
			$image = @imagecreatefromgif($source_url);

   		elseif ($info['mime'] == 'image/png')
   			$image = @imagecreatefrompng($source_url);

		
		// For storage space's sake resize the image's width to 200.
		// Rather than creating a thumbnail for images, for now we'll resize and just
		// upload one since we probably will never display an image > 200 px.
		if(imagesx($image) > 400) {
			$x = imagesx($image);
			$y = imagesy($image);
			$htw = $y/$x;
			$thumb = imagecreatetruecolor(400, 400*$htw);
			
			imagecopyresampled($thumb,$image, 0, 0, 0, 0, 400, 400*$htw,$x, $y);
			imagedestroy($image);
			$image = $thumb;
		}
						
		if(filesize($source_url) > 200000)  // > 200kb
			$quality = 80;
		else {
			$quality = 90;
		}
		imagejpeg($image, $destination_url,$quality);
		
		imagedestroy($image);
		return $destination_url;
	}
}

?>