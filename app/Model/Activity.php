
<?php 
include_once APP.'Object'.DS.'RolledUpActivity.php';

class Activity extends AppModel
{
	public $belongsTo = array('User','Song');
	
	/**
	 * Get the latest activities from the logged in user's followers.
	 * @param limit the number of activities to retrieve
	 * @param offset the number of activities to skip 
	 */
	public function getLatestFollowingActivities($limit = 20,$offset = 0)
	{
		$Follower = ClassRegistry::init('Follower');
		
		$conditionsSubQuery['`follower_id`'] = CakeSession::read("Auth.User.id");
		$dbo = $Follower->getDataSource();
		
		$subQuery = $dbo->buildStatement(    
		    array(        
		        'fields' => array('`followers`.`user_id`'),
	            'table' => $dbo->fullTableName($Follower),
				'alias' => 'followers',
				'conditions' => $conditionsSubQuery), $Follower);
		
	   $subQuery = '`Activity`.`user_id` IN (' . $subQuery . ') ORDER BY `Activity`.`created` DESC LIMIT 20 OFFSET '. $offset;
	   $subQueryExpression = $dbo->expression($subQuery);
	   $conditions[] = $subQueryExpression;
	   
	   return $this->find('all',compact('conditions'));
	}
	
	
	/**
	 * Goes through the given activities and combines activities for the same song,
	 * and then again for the same user.  This happens for every request to get the activities and will not
	 * roll up activities retrieved from different requests i.e Requests for an offset
	 * @param activities the list of activities to roll up.
	 */
	private function rollUpActivites($activities)
	{
		$activityMap = array();
		$activityResultList = array();
		foreach($activities as $activity)
		{
			if(isset($activityMap[$activity['Song']['id']]))
			{
				$rolledUpActivity = $activityMap[$activity['Song']['id']];
				$rolledUpActivity->addActivity($activity);
			}
			else 
			{
				// Create a new rolled up activity, add it to the map of song id,
				// to activity, then add it to the result list
				$rolledUpActivity = new RolledUpActivity();
				$rolledUpActivity->setSong($activity['Song']);
				$activityMap[$activity['Song']['id']] = $rolledUpActivity;
				$rolledUpActivity->addActivity($activity);
				array_push($activityResultList,$rolledUpActivity);
			}
		}

		return $activityResultList;
	}
} 

?>
	