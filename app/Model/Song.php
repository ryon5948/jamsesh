<?php 
class Song extends AppModel
{	
	public $actsAs = array('Containable');
		
	var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'fields' => array('id','stage_name')
        )
    );
	
	var $hasMany = array(
        'Track' => array(
            'className' => 'Track'
        ),
        'TrackRequest' => array(
			'className' => 'TrackRequest'),
		'Comment' => array(
			'className' => 'Comment',
			'foreignKey' => 'foreign_id',
			'conditions' => array('class'=>'song'))
    );
	
	public $validate = array(
        'title' => array(
        	'notEmpty' => array(
                'rule'     => 'notEmpty',
                'required' => "create",
                'message'  => 'This field must be filled out.'
            ),
            'between' => array(
                'rule'    => array('between', 1, 100),
                'message' => 'Between 5 to 15 characters'
            )
        ),
        'description' => array(
            'notEmpty' => array(
                'rule'     => 'notEmpty',
                'required' => "create",
                'message'  => 'This field must be filled out.'
            )
        )
    );
	
	
	// TODO: Heavy refactoring for when tracks will be stored in S3
	public function master()
	{
		$sox = '/opt/local/bin/sox -m ';
		
		$song = $this->data['Song'];
		CakeLog::write('debug','SongMaster'.print_r($this->data,true));
		$tracks = $this->data['Track'];
		
		$workerData = array();
		$songTracks = array();
		if(count($tracks) > 1)
		{
			foreach($tracks as $track)
			{
				$songTracks->push(substr(strrchr($track['url'], "/"), 1));
			}	
		}
		else
			return $tracks[0]['url'];
		
		$uploadFileUtility = ClassRegistry::init('UploadFileUtility');
		$result = $uploadFileUtility->getAudioFilesFromAWS($songTracks);
		
		foreach($result as $filePath):
			$sox .= $filePath . ' ';
		endforeach;
		
		$songTitle = str_replace(' ','_',$song['title']).'.mp3';
		$sox .= $songTitle;
		exec($sox);
		
		// TODO: Upload result songs to S3.  Delete other tracks
		return;
	}

	
	// Sends the S3 work of mastering a song to a Gearman worker.
	private function asynchronousMaster($data)
	{
		# Create our client object.
		$gmclient= new GearmanClient();
		
		# Add default server (localhost).
		$gmclient->addServer();
		
		echo "Sending job\n";
		
		# Send reverse job
		do
		{
		  $result= $gmclient->do("master_song", $data);
		  # Check for various return packets and errors.
		  switch($gmclient->returnCode())
		  {
		    case GEARMAN_WORK_DATA:
		      echo "Data: $result\n";
		      break;
		    case GEARMAN_WORK_STATUS:
		      list($numerator, $denominator)= $gmclient->doStatus();
		      echo "Status: $numerator/$denominator complete\n";
		      break;
		    case GEARMAN_SUCCESS:
		      break;
		    default:
		      echo "RET: " . $gmclient->returnCode() . "\n";
		      exit;
		  }
		}
		while($gmclient->returnCode() != GEARMAN_SUCCESS);
				
	}

	
	// Add an activity entry for song creation
	public function afterSave($created, $options = Array()) 
	{
		$data = Array();
		$data['user_id'] = CakeSession::read("Auth.User.id");
		$data['song_id'] = $this->data['Song']['id'];
		
		$data['action'] = "created this song";
	    $Activity = ClassRegistry::init('Activity');
		$Activity->save($data);
	}
	
	
	public function uploadImage($data,$userId)
	{
		// Move user image
		if( !empty($data['image_url']['name']) )
		{
			$uploadFileUtility = ClassRegistry::init('UploadFileUtility');
			$fileName = $uploadFileUtility->moveImageToS3($data['image_url'],$userId);
			if( !empty($fileName) )
			{
				// The database only contains the filename for the image.
				return $fileName;
			}
		}
	}
}







