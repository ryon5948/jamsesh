<?php 
class TrackRequest extends AppModel
{	
	var $belongsTo = array(
        'Song' => array(
            'className' => 'Song'
        ),
        'User' => array(
			'className' => 'User',
			'foreign_key' => 'user_id')
    );
	
	// Add an activity entry for track request creation
	public function afterSave($created, $options = Array()) 
	{
		$data = Array();
		$data['user_id'] = CakeSession::read("Auth.User.id");
		$data['song_id'] = $this->data['TrackRequest']['song_id'];
		
		$link = "<a href='/trackRequests/view/".$this->data['TrackRequest']['id']."'>%s</a>";
		$data['action'] = "requested that a track be added: " . sprintf($link,$this->data['TrackRequest']['title']);
	    $Activity = ClassRegistry::init('Activity');
		$Activity->save($data);
	}
}
