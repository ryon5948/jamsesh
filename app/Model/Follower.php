<?php
class Follower extends AppModel {
	
	public $belongsTo = array(
		'UserFollowers' => array(
			'className'=>'User',
			'foreignKey' => 'follower_id',
			'counterCache' => 'following_count'),
		'UserFollowing' => array(
			'className'=>'User',
			'foreignKey' => 'user_id',
			'counterCache' => 'follower_count'));
	}
?>