<?php  
class Comment extends AppModel { 

	// Default this to tracks comments
	
    var $actsAs = array('Containable'); 
	
	var $belongsTo = array(
		'Track' => array(
			'className'=>'Track',
			'foreignKey' => 'foreign_id',
			'counterCache' => 'track_comment_count'),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'),
		'Song' => array(
			'className'=>'Song',
			'foreignKey' => 'foreign_id',
			'counterCache' => 'song_comment_count'));
			
	public $validate = array(
        'content' => array(
            'notEmpty' => array(
                'rule'     => 'notEmpty',
                'required' => "create",
                'message'  => 'This field must be filled out.'
            )
        )
    );
	
	public function getCommentsForModel($modelName,$modelId,$offset = 0)
	{	
		$this->contain('User');
		$comments = $this->find('all',
			array(
				'conditions'=>array(
					'foreign_id'=>$modelId,
					'class'=>$modelName),
				'offset' => $offset,
				'limit' => 20,
				'order' => array('Comment.created'=>'DESC')));
				
		return $comments;
	}

} 
?>