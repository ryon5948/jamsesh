<?php  
class Like extends AppModel { 

	// Default this to tracks comments
	
    var $actsAs = array('Containable'); 
	
	var $belongsTo = array(
		'Track' => array(
			'className'=>'Track',
			'foreignKey' => 'foreign_id',
			'counterCache' => 'track_like_count'),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'),
		'Song' => array(
			'className'=>'Song',
			'foreignKey' => 'foreign_id',
			'counterCache' => 'song_like_count'));
			
	
	public function getLikesForModel($modelName,$modelId,$offset = 0)
	{	
		$this->contain('User');
		$comments = $this->find('all',
			array(
				'conditions'=>array(
					'foreign_id'=>$modelId,
					'class'=>$modelName),
				'offset' => $offset,
				'limit' => 20,
				'order' => array('Comment.created'=>'DESC')));
				
		return $comments;
	}
	
	
	// Add an activity entry for track creation
	public function afterSave($options = array()) 
	{
		$data = Array();
		$data['user_id'] = CakeSession::read("Auth.User.id");
		
		// Add an activity only for song "likes" for now
		if($this->data['Like']['class'] == 'song'){
			$data['song_id'] = $this->data['Like']['foreign_id'];
			$data['action'] = "likes this";
			$Activity = ClassRegistry::init('Activity');
			$Activity->save($data);	
		}
	}
} 
?>