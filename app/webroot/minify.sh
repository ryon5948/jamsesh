#!/bin/bash

if [[ -z $1 ]]; then echo "Usage: <filename>";exit;fi

file=$1
filename=${file%.*}
ext=${file##*.}

echo "Minifying to file $filename.min.$ext"

grep "" $file >> $filename.min.$ext
