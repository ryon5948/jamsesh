function TracksController() 
{
 	var trackList = [];
 	var intervalId;
 	var progressBar;
 	
	if ( arguments.callee._singletonInstance )
		return arguments.callee._singletonInstance;
	arguments.callee._singletonInstance = this;
	
	// Initialize soundManager
	soundManager.setup({
	  url: '/swf',
	  onready: function() {
	    
	    if(!trackList)
	    {
	    	console.log("No tracks to initialize");
	    	return;
	    }
	    // Create a sound manager audio instance
	    // for all tracks in the song.
	    trackList.map( function(track) {
	    	track.setAudio(soundManager.createSound({
		      id: 'audio_' + track.getRowId(),
		      url: track.getUrl()
		    }));
	    });
	  },
	  ontimeout: function() {
	    // Hrmm, SM2 could not start. Missing SWF? Flash blocked? Show an error, etc.?
	  }
	});

	/**
	 * Plays all tracks.  Tracks that are "selected" will have volume set
	 * to 100, where as unselected tracks will be set at 0.  This way you can select 
	 * and deselect tracks while they're playing and they will stay in sync with the other songs.
	 */
	this.playTracks = function() 
	{
    	
		// This is sort of a hack.
    	// If there were tracks in the list, set the progress bar for the first track
    	// and let that update the progress bar from now on.
    	if( trackList.length )
    	{
    		trackList[0].setProgressBar($('#progress-bar'));
    	}
    	
		for( var i = 0; i < trackList.length; i++ )
    	{
    		trackList[i].play();
    	}
    }
	
	// Pause all tracks
	this.pauseTracks = function() 
	{	
    	soundManager.pauseAll();
    }
	
	this.resetTracks = function() 
	{
		for( var i = 0; i < trackList.length; i++ )
    	{
    		trackList[i].reset();
    	}
	}
    
    this.addTrack = function( track )
    {
    	trackList.push(track);
    }
}

// Init a global variable
var tracksController = new TracksController();