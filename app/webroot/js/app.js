var jamseshApp = angular.module('jamseshApp', [
  'ngRoute',
  'jamseshControllers'
]);

/*
* Wrap ajax calls to handle errors/logging etc...
*/
!function(send){
    XMLHttpRequest.prototype.send = function(data){
    	
    	this.onerror = function(){
    		alert("An error occurred");
    	};
    	
    	this.onload = function(data){
    		console.log(data);
    	}
    	
        send.call(this, data);
        console.log("success");
    }
}(XMLHttpRequest.prototype.send);


/*
 * Utility functions
 */
jamseshApp.helpers = {
	toJsDate: function(str) {
	    if(!str) return null;
	    return new Date(str);
	  }
}

jamseshApp.config(['$routeProvider','$locationProvider',
    function($routeProvider,$locationProvider) {
      $routeProvider.
        when('/', {
            templateUrl: 'views/home.html',
            controller: 'homeController'
          }).
        when('/users', {
          templateUrl: 'views/users.html',
          controller: 'usersController'
        }).
        when('/explore', {
            templateUrl: 'views/genres.html',
            controller: 'genresController'
          }).
        when('/songs/view/:id', {
          templateUrl: 'views/song.html',
          controller: 'songController'
        }).
        when('/users/login', {
    	  templateUrl: 'views/login.html',
    	  controller: 'loginController'
      	}).
      	 when('/songs/add', {
	   	  templateUrl: 'views/songCreateForm.html',
	   	  controller: 'songCreateFormController'
     	}).
        when('/dashboard', {
            templateUrl: 'views/dashboard.html',
            controller: 'dashboardController'
          });
      
	  // use the HTML5 History API
      $locationProvider.html5Mode(true);
    }]);

jamseshApp.filter('unsafe', function($sce) {
    return function(val) {
    	// If somebody entered a script tag, ignore it.
    	if(val.toLowerCase().indexOf('<script>') < 0)
    		return $sce.trustAsHtml(val);
    };
});

var genres = "pop,rock,metal,jazz,blues,classical,country,acapella,other";