// create the module and name it jamseshApp
        // also include ngRoute for all our routing needs
	var jamseshApp = angular.module('jamseshApp', ['ngRoute']);

	// configure our routes
	jamseshApp.config(function($routeProvider) {
		$routeProvider

			// route for the home page
			.when('users', {
				templateUrl : '../views/users.html',
				controller  : 'usersController'
			})

			// route for the about page
			.when('/about', {
				templateUrl : 'pages/about.html',
				controller  : 'aboutController'
			})

			// route for the contact page
			.when('/contact', {
				templateUrl : 'pages/contact.html',
				controller  : 'contactController'
			});
	});
	
	jamseshApp.controller('usersController', function($scope) {
		// create a message to display in our view
		$scope.message = 'Everyone come and see how good I look!';
	});
