/**
 * Main script file for Jamsesh.
 * 
 * @author Andrew Ryon
 */


function postComment(formData,success)
{
	if(formData)
	{
		$.ajax({
			url:'/comments/add', 
			data: formData,
			type:'POST',
			success: function(response){ 
				success(response); 
			}
		});
	}
}


function masterSong(songId,success)
{
	if(songId)
	{
		$.ajax({
			url:'/songs/master', 
			data: {songId:songId},
			type:'POST',
			success: function(response){ 
				success(response); 
			}
		});
	}
}


function readURL(imageId,input) {
    if (input.files && input.files[0]) {
    	// Reject if image is > 4MB.
    	if(input.files[0].size > 4000000) {
    		console.warn('File size ' + input.files[0].size + ' is too large');
    		alert('Please select a file that is less than 4MB.')
    		return false;
    	}
    	
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#' + imageId).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        return true;
    }
}

var ajaxController = (function(){
	
	var url;
	var data;
	var type;
	var success_handler;
	var error_handler;
	
	// Initializes variables required to retrieve the data.
	function init( url, type, data, onSuccess, onError )
	{
		this.url = url;
		this.type = type;
		this.data = data;
		success_handler = onSuccess;
		error_handler = onError;
		
		if( !this.url || !this.type )
		{	
			logRequestData();
			return false;
		}
		
		// Set empty success/event handlers
		if( !onError ) {
			error_handler = logRequestData;
		} 
		if( !onSuccess ) {
			success_handler = logRequestData;
		}
		
		return true;
	}
	
	
	function logRequestData()
	{
		console.log( "Request data \n" 
				+ "url: " + this.url + "\n"
				+ "type: " + this.type + "\n"
				+ "data: " + this.data );
	}
	
	
	// Make the ajax call
	function sendRequest() {
		
		$.ajax({
			url: this.url, 
			data: this.data,
			type: this.type,
			success: function(response){ 
				success_handler(response); 
			},
			error: function(response) {
				error_handler(response);
			}
		});
		
	}
		
	//
	return {
		
		sendRequest: function( url, type, data, onSuccess, onError ) {
			
			// Initialize variables.
			if( !init( url, type, data, onSuccess, onError ) )
				return;
			
			sendRequest();
		}
	}
	
})();
