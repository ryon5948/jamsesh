var jamseshControllers = angular.module('jamseshControllers', []);

/**
 * UsersController - Show a list of all users.
 */
jamseshControllers.controller('usersController', ['$scope', '$http',
  function ($scope,$http) {
	
	// Retrieve the list of users
	$http.get('/users.json').success(function(data) {
	     $scope.users = data.users;
	   });
	 
	// Follow
	$scope.follow = function(userId){
		$http({
		    method: 'POST',
		    url: '/followers/follow/',
		    data: { userId: userId }
		}).success(function(data){
			showModal("Congradulations!", "You are now following this person");
		}).error(function(){
			showModal("Error","You are already following this person");
		});
	};
	
	// Function for sending a message to another user.
	$scope.message = function(user){
		// 
	};
  }]);


/**
 * Show the home page.
 */
jamseshControllers.controller('homeController', ['$scope',
  function ($scope) {
    
    $scope.message = 'This is my first message';
  }]);


/**
 * Shows the recent activities for a user
 */
jamseshControllers.controller('dashboardController', ['$scope', '$http',
 function ($scope,$http) {
   
	var rollUp = {};
	var activities = [];
	$scope.helpers = jamseshApp.helpers;
	
	// Retrieve the list of the users recent activity
	$http.get('/activities.json?offset=0').success(function(data) {
	     activities = data.activities;
	
		// Roll up all activities by the song.  So that only 
		// one element will be shown for each song.
		for( index in activities )
		{
			var activity = activities[index];
			var rollUpActivity = rollUp[activity.Song.id];
			
			if( rollUpActivity != null )
			{
				rollUpActivity.push(activity);
			}
			else
			{
				rollUp[activity.Song.id] = [activity];
			}
		}
		
		$scope.rollUp = rollUp;
		
		// Retrieve the top ten songs for the user
		$http.get('/users/songs/10.json').success(function(data){
			$scope.songs = data.user;
		});
	
	});
 }]);


/**
 * Display all songs in a certain genre
 */
jamseshControllers.controller('genresController', ['$scope','$http','$location',
  function ($scope,$http,$location) {
    
	var genre = $location.search()['genre'];
	
	// Retrieve the list of users
	$http.get('/explore.json?genre=' + genre).success(function(data) {
	     $scope.songs = data.songs;
	   });
	
  }]);


/**
 * Display all information for a song
 */
jamseshControllers.controller('songController', ['$scope', '$routeParams','$http',
  function($scope, $routeParams, $http) {
	
    var id = $routeParams.id;
    $scope.helpers = jamseshApp.helpers;
    
    // Fetch song data
    $http({
    	url:'/songs/view/'+id+'.json',
    	method:'GET'
    }).success(function(data) {
	     $scope.song = data.song;
	     $scope.tracks = data.tracks;
	     $scope.trackRequests = data.trackRequests;
	     $scope.creator = data.creator;
   });
    
    // Fetch comments for the song
	$http({
		url: '/comments/view/song/' + id + '.json?offset=0',
		method: 'GET'
	}).success(function(data){
		$scope.comments = data.comments;
	});
	
}]);


/**
 * Sign in and authenticate the user
 */
jamseshControllers.controller('loginController', ['$scope','$http',
  function($scope, $http) {

	$scope.login = function(username,password){
		$http({
			url: '/users/login',
			method: 'POST',
			data: {
				User: {
					username: username,
					password: password
				}
			}
		}).success(function(data){
			window.location.href = "/activities";
		});
	};
     
  }]);


/**
 * Handles the form for song creation
 */
jamseshControllers.controller('songCreateFormController', ['$scope','$http',
  function($scope, $http) {

	$scope.login = function(title,description,genre,image){
		$http({
			url: '/songs/add',
			method: 'POST',
			data: {
				User: {
					username: username,
					password: password
				}
			}
		}).success(function(data){
			window.location.href = "/activities";
		});
	};
     
  }]);