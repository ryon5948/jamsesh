
function Track(title, id, initialSelect, url)
{
	var title = title;
	var id = id;
	var url = url;
	var progressBar;
	
	// Sound manager audio instance
	var audio;
	var intervalId;
	
	$('#'+id).hover(function(){
		$(this).find('.deleteTrack').toggleClass('hide');
		$(this).find('.addTrack').toggleClass('hide');
	});
	
	var isSelected = (initialSelect == "true") ? true: false;
	
	
	function updateProgressBar(isPlay) {
		
		if(!progressBar) {
			return;
		}
		
		if(!isPlay) {
    		clearInterval(intervalId);
    		return;
    	}
    	
    	if(isPlay) {
	    	progressBar.width( (audio.position / audio.duration) * 100 + '%' );
    	}
	}
	
	this.toggleSelect = function() {
		isSelected = isSelected ? false: true;
		$( '#' + id + '-volume-on').toggleClass('hidden');
		$('#' + id + '-volume-off').toggleClass('hidden');
		
		// Toggle volume
		audio.setVolume( ( audio.volume == "100" ) ? "0":"100" );
	};
	
	this.isSelected = function() {
		return isSelected;
	};
	
	this.getRowId = function(){ return id; };
	this.getUrl = function(){ return url; };
	this.getTitle = function(){ return title; };
	
	this.setAudio = function(soundManagerAudio){ 
		audio = soundManagerAudio; 
		audio.setVolume( isSelected ? "100":"0" );
	}
	
	// Play track and update progress bar.
	this.play = function() {
		if(audio) {
			audio.play();
			intervalId = setInterval(function(){updateProgressBar(true)}, 5);
			updateProgressBar(true);
		}
	}
	
	// Pause track and stop updating progress bar.
	this.pause = function() {
		if(audio) {
			audio.pause();
			updateProgressBar(false);
		}
	}
	
	this.reset = function() {
		if(audio) {
			audio.setPosition(0);
		}
	}
	
	// For individual tracks an optional progress bar can be supplied to have that
	// updated while the song is playing.  For unfinished songs, the tracksController will handle 
	// the song's progress bar as a whole.
	this.setProgressBar = function(pProgressBar) {
		progressBar = pProgressBar;
	}
}